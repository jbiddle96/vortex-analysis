import numpy as np
import matplotlib as mpl
from glob import glob
from math import floor, log10
import yaml
import os
import shutil

mpl.use('Agg')
mpl.rc('text', usetex=True)
mpl.rc('font', family='serif')
mpl.rcParams['axes.unicode_minus'] = False
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}')

default_values = {'mu': 5.5,
                  'a': 0.1,
                  'm_pi': None}


def load_config(filename='config.yml'):
    """
    Load config file

    """
    print(f"Loading configuration file: {filename}")

    with open(filename, 'r') as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)

    general = config.pop('general', {})

    for key in config:
        config[key].setdefault('label', key)

    return config, general


def load_metadata(filename, type):

    if filename is not None:
        with open(filename, "r") as file:
            metadata = yaml.load(file, Loader=yaml.FullLoader)
    else:
        metadata = default_values
        return metadata

    if not type:
        raise ValueError('type must be specified')

    metadata = metadata[type]

    # Assign any missing keys the default value
    for key in default_values:
        metadata.setdefault(key, default_values[key])

    return metadata


def assign_dir(dir_name, clean=False, clean_dirs=False):
    """Assign a directory and ensure it exists

    :param dir_name: The directory to assign
    :param clean: (Optional, default=False) Empty the directory
    :returns dir_name:

    """
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    if clean:
        for filename in os.listdir(dir_name):
            file_path = os.path.join(dir_name, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path) and clean_dirs:
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    return dir_name


def find_files(base_dir, pattern):
    """Find files in a directory according to pattern

    :param base_dir: Directory to search
    :param pattern: Glob pattern to match
    :returns file_list: List of matched files

    """

    full_pattern = os.path.join(base_dir, pattern)
    file_list = glob(full_pattern)
    file_list.sort()
    return file_list


def save_yaml(data, filename, parse_floats=True):
    if parse_floats:
        modify_dict(data, float)
    with open(filename, 'w') as f:
        yaml.dump(data, f, sort_keys=False)


def round_sf(num, sf=1):
    """Round to a specified number of significant figures"""
    return round(num, (sf - 1) - floor(log10(abs(num))))


def mantissa_and_exponent(num):
    """Find the mantissa and exponent of num"""
    e = floor(log10(abs(num)))
    m = num / (10 ** e)
    return m, e


def format_error(val, err):
    """
    Format a number and it's error to read x.x(e).
    Formatting is based on the highest significant digit of the error.
    """
    val_m, val_e = mantissa_and_exponent(val)
    err_m, err_e = mantissa_and_exponent(err)
    if err_e > val_e:
        print('Error is more significant than value, aborting')
        return ''

    diff_e = err_e - val_e

    # Round value to the most significant digit of err
    val = round(val_m, -diff_e) * 10 ** val_e
    # Round err to its most significant digit
    err = round(err, -err_e)

    # Get the most significant digit of err as an integer
    msd_err = round(err / (10**floor(log10(err))))

    if(err_e < 1):
        string = '{num:0.{dp}f}'.format(num=val, dp=-err_e) + f'({msd_err})'
    else:
        string = '{num:0.{dp}e}'.format(num=val, dp=-diff_e) + f'({msd_err})'
    return string


def modify_dict(d, func=lambda x: x):
    """Apply function to all non-dict elements of nested dictionaries

    Acts in-place on d

    :param d: Dictionary to modify
    :param func: Function to apply (default is the identity function)

    """
    for k, v in d.items():
        if isinstance(v, dict):
            modify_dict(v, func)
        else:
            d[k] = func(v)
