"""Analyse graph structures"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import numpy as np
import importlib
import time
from tabulate import tabulate
from cache import cache
import vis_helper as vis
import lattice_types as lt
import make_table as mt
importlib.reload(vis)
importlib.reload(lt)
importlib.reload(mt)

mpl.rc('text', usetex=True)
mpl.rc('font', family='serif', size=16)
mpl.rcParams['axes.unicode_minus'] = False
mpl.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')

if __name__ == '__main__':

    config, general = vis.load_config()
    clean = general.get('clean', False)
    meta_file = general.get('meta_file', None)
    min_rate_size = general.get('min_rate_size', 0)
    max_dist = general.get('max_dist', np.infty)
    default_width = 6.4
    default_height = 4.8
    ncols, nrows = 2, 3

    cache_dir = vis.assign_dir('./cache', clean)

    combined_figures = {}
    paper_figures = {}
    combined_dir = vis.assign_dir('combined_graph_analysis', clean)

    colours = plt.rcParams['axes.prop_cycle'].by_key()['color']
    colours = list(colours)

    # cmap = plt.get_cmap("jet")
    # n_ensemble = len(config)
    # alt_colours = cmap(np.linspace(0, 1.0, int(n_ensemble)))

    stats_primary = {}
    stats_all = {}

    for i, ensemble in enumerate(config):
        cfg = config[ensemble]
        graph_list = cfg['graph_list']
        dist_list = cfg['dist_list']

        type = cfg.get('type', ensemble)
        metadata = vis.load_metadata(meta_file, type)
        name = cfg.get('name', ensemble)
        output_dir = f'{name}_graph_analysis'
        output_dir = vis.assign_dir(output_dir, clean)
        cfg['output_dir'] = output_dir

        label = cfg.get('label', ensemble)
        colour = colours[i]
        fit_colour = colours[i]

        # Load the ensemble
        start_time = time.perf_counter()
        graph_ensemble = lt.GraphEnsemble(graph_list, metadata)
        dist_ensemble = lt.DistEnsemble(dist_list, metadata)
        end_time = time.perf_counter()
        print(f'Loaded ensemble: {ensemble}')
        print(f'Loading took {end_time-start_time} seconds')

        start_time = time.perf_counter()
        for p_only, p_key in zip([True, False], ['primary', 'all']):
            dist_stats = dist_ensemble.get_dist_statistics(primary_only=p_only)
            if p_only:
                stats_primary[ensemble] = dist_stats
            else:
                stats_all[ensemble] = dist_stats
            formatted_table = tabulate(dist_stats,
                                       headers='keys',
                                       tablefmt='psql')
            filename = os.path.join(output_dir, f'stats_{p_key}.txt')
            with open(filename, 'w') as f:
                f.write(formatted_table)

        end_time = time.perf_counter()
        print('Calculated statistics')
        print(f'Statistics calculation took {end_time-start_time} seconds')

        naive_rate = cfg.get('naive_rate')
        if naive_rate is not None:
            alt_colour = cfg.get('alt_colour')

        # Loop over t_hat values
        for t_hat in dist_ensemble.t_hat_values:

            if t_hat not in combined_figures:
                combined_figures[t_hat] = plt.subplots(constrained_layout=True,
                                                       ncols=ncols,
                                                       nrows=nrows,
                                                       figsize=(ncols * default_width,
                                                                nrows * default_height))
                paper_figures[t_hat] = plt.subplots(constrained_layout=True,
                                                    ncols=2,
                                                    figsize=(2 * default_width,
                                                             default_height))

            fig_comb, ax_comb = combined_figures[t_hat]
            fig_paper, ax_paper = paper_figures[t_hat]

            fig, axes = plt.subplots(nrows=nrows, ncols=ncols,
                                     figsize=(ncols * default_width,
                                              nrows * default_height),
                                     constrained_layout=True)
            fig_norm, ax_norm = plt.subplots(constrained_layout=True)
            fig_norm_log, ax_norm_log = plt.subplots(constrained_layout=True)

            dist_stats = stats_all[ensemble]
            rate_exact = dist_stats.at['rate_lat', f'{t_hat}']

            for ax in [axes, ax_comb]:
                title = 'Average BP separation'
                dist_ensemble.plot_hist(ax[0, 0], t_hat,
                                        title=title,
                                        color=colour, label=label,
                                        cutoff=max_dist)

                title = 'Average BP separation (log scale)'
                dist_ensemble.plot_hist(ax[0, 1], t_hat,
                                        title=title, log=True,
                                        color=colour, label=label,
                                        cutoff=max_dist)

                title = 'Average BP separation (Normalised)'
                dist_ensemble.plot_hist(ax[1, 0], t_hat,
                                        title=title, density=True,
                                        color=colour, label=label,
                                        cutoff=max_dist)

                title = 'Average BP separation (Normalised, log scale)'
                dist_ensemble.plot_hist(ax[1, 1], t_hat,
                                        title=title, log=True, density=True,
                                        color=colour, label=label,
                                        cutoff=max_dist)

                title = 'Average BP separation (Normalised, functional fits)'
                dist_ensemble.plot_hist(ax[2, 0], t_hat,
                                        plot_err=True,
                                        title=title, density=True,
                                        color=colour, label=label,
                                        cutoff=max_dist)

                dist_ensemble.plot_func(ax[2, 0], t_hat, rate_exact,
                                        color=fit_colour,
                                        cutoff=max_dist,
                                        label=f'Exact, {label}')
                if naive_rate is not None:
                    dist_ensemble.plot_func(ax[2, 0], t_hat,
                                            naive_rate,
                                            color=alt_colour,
                                            cutoff=max_dist,
                                            label=f'Naive, {label}')

                title = 'Average BP separation (Normalised, functional fits, log scale)'
                dist_ensemble.plot_hist(ax[2, 1], t_hat,
                                        plot_err=False,
                                        title=title,
                                        log=True,
                                        density=True,
                                        color=colour,
                                        label=label,
                                        cutoff=max_dist)

                dist_ensemble.plot_func(ax[2, 1], t_hat,
                                        rate_exact,
                                        color=fit_colour,
                                        cutoff=max_dist,
                                        label=f'Exact, {label}')
                if naive_rate is not None:
                    dist_ensemble.plot_func(ax[2, 1], t_hat,
                                            naive_rate,
                                            color=alt_colour,
                                            cutoff=max_dist,
                                            label=f'Naive, {label}')

                for a in ax.flatten():
                    a.legend()

            filename = f'dist_hist_t{t_hat}.pdf'
            filename = os.path.join(output_dir, filename)
            fig.savefig(filename)

            # Make single plot for paper
            dist_ensemble.plot_hist(ax_paper[0], t_hat,
                                    plot_err=True,
                                    density=True,
                                    color=colour,
                                    label=label,
                                    cutoff=max_dist,
                                    xlabel='BP Separation',
                                    ylabel=r'$P(X=k)$')

            dist_ensemble.plot_func(ax_paper[0], t_hat, rate_exact,
                                    color=fit_colour,
                                    cutoff=max_dist,
                                    label=f'Exact, {label}')

            dist_ensemble.plot_hist(ax_norm, t_hat,
                                    plot_err=True,
                                    density=True,
                                    color=colour,
                                    label=label,
                                    cutoff=max_dist,
                                    xlabel='BP Separation',
                                    ylabel=r'$P(X=k)$')

            dist_ensemble.plot_func(ax_norm, t_hat, rate_exact,
                                    color=fit_colour,
                                    cutoff=max_dist,
                                    label=r'$F(\hat{q},\,k)$')
            if naive_rate is not None:
                dist_ensemble.plot_func(ax_norm, t_hat,
                                        naive_rate,
                                        color=alt_colour,
                                        cutoff=max_dist,
                                        label=r'$F(q_{\rm n{\ddot{a}}ive},\,k)$')

            dist_ensemble.plot_hist(ax_paper[1], t_hat,
                                    plot_err=False,
                                    log=True,
                                    density=True,
                                    color=colour,
                                    label=label,
                                    cutoff=max_dist,
                                    xlabel='BP Separation',
                                    ylabel=r'$P(X=k)$')

            dist_ensemble.plot_func(ax_paper[1], t_hat,
                                    rate_exact,
                                    color=fit_colour,
                                    cutoff=max_dist,
                                    label=f'Exact, {label}')

            dist_ensemble.plot_hist(ax_norm_log, t_hat,
                                    plot_err=False,
                                    log=True,
                                    density=True,
                                    color=colour,
                                    label=label,
                                    cutoff=max_dist,
                                    xlabel='BP Separation',
                                    ylabel=r'$P(X=k)$')

            dist_ensemble.plot_func(ax_norm_log, t_hat,
                                    rate_exact,
                                    color=fit_colour,
                                    cutoff=max_dist,
                                    label=r'$F(\hat{q},\,k)$')

            if naive_rate is not None:
                dist_ensemble.plot_func(ax_norm_log, t_hat,
                                        naive_rate,
                                        color=alt_colour,
                                        cutoff=max_dist,
                                        label=r'$F(q_{\rm n{\ddot{a}}ive},\, k)$')

            ax_norm_log.legend()
            ax_norm.legend()
            filename = f'dist_hist_t{t_hat}_single.pdf'
            filename = os.path.join(output_dir, filename)
            fig_norm.savefig(filename)

            filename = f'dist_hist_t{t_hat}_single_log.pdf'
            filename = os.path.join(output_dir, filename)
            fig_norm_log.savefig(filename)

    for combined_plots, lbl in zip([combined_figures, paper_figures],
                                   ['multi', 'single']):
        for t_hat in combined_plots:
            fig, axes = combined_plots[t_hat]
            for ax in axes.flatten():
                ax.legend()
            filename = f'dist_hist_t{t_hat}_{lbl}.pdf'
            filename = os.path.join(combined_dir, filename)
            fig.savefig(filename)

    # Cache stats
    filename = os.path.join(cache_dir, 'stats_all.pickle')
    cache(stats_all, cache_name=filename)
    filename = os.path.join(cache_dir, 'stats_primary.pickle')
    cache(stats_primary, cache_name=filename)
