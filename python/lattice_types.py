"""
Classes used for loading and analysing ensembles
"""
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import re
import time
import os
import networkx as nx
from pyvis.network import Network

# |------------------------ Ensemble classes ------------------------|


class BaseEnsemble(dict):
    def __init__(self, file_list, metadata=None):
        # Initialise self as a dictionary
        super().__init__()

        self.file_list = self._load_file_list(file_list)

        # Assign dictionary of configurations belonging to the ensemble
        self.load_configurations()

        # Find the unique t_hat values present across data
        self.t_hat_values = list(self._find_t_hat_values())

        # Set the number of configurations and the size of the lattice
        self.n_config = len(self)
        self.dims = self._set_dims()
        self.nd = len(self.dims)

        # Store lattice info
        if metadata is None:
            self.info = {}
        else:
            self.info = metadata

    def _load_file_list(self, file_list):
        # If a list of files is passed, return it
        if isinstance(file_list, list):
            return file_list

        elif isinstance(file_list, str):
            # Else interpret name as a filename to be opened
            with open(file_list, 'r') as f:
                lines = f.read().splitlines()

            file_list = sorted(lines[1:])
            return file_list
        else:
            raise TypeError(f'File list must be of type list or str, not type {type(file_list)}')

    def _set_dims(self):
        """Set the lattice dimensions

        :returns: Numpy array of the lattice dimensions
        """

        # Check if all configurations have the same dimension
        all_dims = np.array([self[key].dims for key in self])
        all_equal = np.all(np.all(all_dims == all_dims[0, :], axis=0))
        if not all_equal:
            raise ValueError('Configurations do not all agree on lattice dimensions')

        return all_dims[0, :]

    def _find_t_hat_values(self):
        first_key = list(self.keys())[0]
        t_hat_values = set(self[first_key].data.keys())
        for key in self:
            this_values = set(self[key].data.keys())
            t_hat_values = t_hat_values.intersection(this_values)

        return t_hat_values

    def slice_dims(self, t_hat):
        """Get the dimensions of a 3D slice

        :param t_hat: Direction being sliced along
        :returns: numpy array of slice dimensions

        """

        return np.array([d for i, d in enumerate(self.dims)
                         if i != t_hat - 1])

    def load_configurations(self):
        raise NotImplementedError()

    def analyse_samples(self, samples):
        """Analyse an array of samples to produce ensemble statistics

        :param samples: Array of samples
        :returns: Value and error row for insertion into a dataframe

        """

        slice_avg = np.mean(samples, axis=2, where=(samples > -1))
        spatial_avg = np.mean(samples[:, 0:3, :], axis=(1, 2),
                              where=(samples[:, 0:3, :] > -1))
        total_avg = np.mean(slice_avg, axis=1)

        t_hat = np.mean(slice_avg, axis=0)
        t_hat_err = np.std(slice_avg, axis=0, ddof=1) / np.sqrt(self.n_config)

        spatial = np.mean(spatial_avg)
        spatial_err = np.std(spatial_avg, ddof=1) / np.sqrt(self.n_config)

        total = np.mean(total_avg)
        total_err = np.std(total_avg, ddof=1) / np.sqrt(self.n_config)

        val_row = t_hat.tolist()
        val_row.append(spatial)
        val_row.append(total)

        err_row = t_hat_err.tolist()
        err_row.append(spatial_err)
        err_row.append(total_err)

        return val_row, err_row


class LoopEnsemble(BaseEnsemble):
    def __init__(self, file_list, metadata=None, load_extents=True):
        self._load_extents = load_extents
        super().__init__(file_list, metadata)

    def load_configurations(self):
        loop_pattern = re.compile('(\S*)-t(\d)\S*\.loops')
        self.extent_files = {}
        for filename in self.file_list:
            # Find unique ensembles and group together the different t-hat files
            pre, ext = os.path.splitext(filename)
            extent_file = pre + '.extent'
            match = re.match(loop_pattern, filename)
            key = match[1]
            t_hat = int(match[2])
            if self.get(key) is None:
                self[key] = {}
                self.extent_files[key] = {}

            self[key][t_hat] = filename
            self.extent_files[key][t_hat] = extent_file

        # Store configuration data as a dict of configuration objects
        for key in self:
            if self._load_extents:
                self[key] = LoopConfiguration(self[key],
                                              self.extent_files[key])
            else:
                self[key] = LoopConfiguration(self[key])

    def get_cluster_statistics(self):
        """Calculate the mean and standard devation
        of the total, primary and secondary loop lengths.

        :returns result: Dictionary containing the resultant statistics

        """
        columns = [str(i) for i in self.t_hat_values]
        columns.extend(['spatial', 'total'])

        rows = ['N_total', 'N_total_err',
                'N_primary', 'N_primary_err',
                'N_secondary', 'N_secondary_err']

        max_dim = np.amax(self.dims)
        total_samples = -np.ones((self.n_config, self.nd, max_dim))
        primary_samples = -np.ones((self.n_config, self.nd, max_dim))
        secondary_samples = -np.ones((self.n_config, self.nd, max_dim))

        # Populate the sample arrays
        for icfg, config in enumerate(self.values()):
            primary_full = config.primary
            secondary_full = config.secondary
            for t, t_hat in enumerate(self.t_hat_values):
                primary = primary_full[t_hat]
                secondary = secondary_full[t_hat]
                for ix, x in enumerate(primary):
                    p_slice = primary[x]
                    s_slice = secondary[x]
                    p_length = p_slice['loop'].value_counts()
                    s_length = s_slice['loop'].value_counts()

                    p_sample = p_length.iloc[0]
                    if not s_length.empty:
                        s_sample = s_length.mean()
                    else:
                        s_sample = 0.0

                    t_sample = p_sample + s_length.sum()

                    total_samples[icfg, t, ix] = t_sample
                    primary_samples[icfg, t, ix] = p_sample
                    secondary_samples[icfg, t, ix] = s_sample

        total_val_row, total_err_row = self.analyse_samples(total_samples)
        primary_val_row, primary_err_row = self.analyse_samples(primary_samples)
        secondary_val_row, secondary_err_row = self.analyse_samples(secondary_samples)

        data = [total_val_row,
                total_err_row,
                primary_val_row,
                primary_err_row,
                secondary_val_row,
                secondary_err_row]

        df = pd.DataFrame(data, columns=columns, index=rows)
        return df

    def find_large_secondary_loops(self, size, logfile):
        """Find configurations with secondary loops equal to or larger than size

        :param size: Loop size to exceed
        :param logfile: Filename to output results to

        """
        f = open(logfile, 'w')
        for key in self:
            config = self[key]
            secondary = config.secondary
            for t_hat in secondary:
                slices = secondary[t_hat]
                for t in slices:
                    slice = slices[t]
                    s_length = slice['loop'].value_counts()
                    max_size = np.amax(s_length)
                    if max_size >= size:
                        string = f'{key}\n t_hat: {t_hat}, slice: {t}, size: {max_size}\n\n'
                        f.write(string)
        f.close()

    def make_loop_hist(self, t_hat, keep_primary=False, normalise=False,
                       min_size=0, max_size=np.infty):
        """Make a histogram of loop lengths

        :param t_hat: Which direction is sliced along
        :param keep_primary: (Optional, default=False)
        Toggle whether to include the primary loop
        :param normalise: (Optional, default=False)
        Choose whether to normalise by lattice volume ('lattice'),
        physical volume ('physical') or total vortex number ('vortex')
        :returns: (bins, histogram values)

        """
        values = []
        n_vortices = 0
        # Loop through each configuration
        for key in self:
            config = self[key]
            primary, secondary = config.primary, config.secondary
            primary = primary[t_hat]
            secondary = secondary[t_hat]
            # Loop through each time slice
            for t in primary:
                p_slice = primary[t]
                s_slice = secondary[t]

                # Keep only loops with desired size
                p_length = p_slice['loop'].value_counts()
                mask = (p_length <= max_size) & (p_length >= min_size)
                p_length = p_length[mask].tolist()

                s_length = s_slice['loop'].value_counts()
                mask = (s_length <= max_size) & (s_length >= min_size)
                s_length = s_length[mask].tolist()

                n_vortices += len(config.data[t_hat][t])

                # Append lengths to the values array
                # Also keep track of the number of vortices
                if keep_primary:
                    values.extend(p_length)

                values.extend(s_length)

        if not values:
            raise ValueError('No values for loop histogram')

        # Calculate the histogram
        if max_size == np.infty:
            max_size = np.amax(values)
        bins = np.arange(0.5, max_size + 1.5)
        hist_counts, _ = np.histogram(values, bins=bins)
        err = np.sqrt(hist_counts)
        hist_counts = hist_counts / self.n_config
        err = err / self.n_config

        # Apply normalisation
        if normalise == 'lattice':
            hist_counts = hist_counts / np.prod(self.dims)
            err = err / np.prod(self.dims)
        elif normalise == 'physical':
            a = self.info.get('a', 1)
            V = np.prod(self.dims) * a ** 4
            hist_counts = hist_counts / V
            err = err / V
        elif normalise == 'vortex':
            hist_counts = hist_counts / n_vortices
            err = err / n_vortices

        return hist_counts, err, bins

    def bin_cluster_extents(self, t_hat, nbins, normalise=False):
        """Generate a histogram of the cluster extents

        :param t_hat: Dimension to slice along
        :param nbins: number of bins for the histogram
        :returns: bins, counts

        """
        bins = np.linspace(0, 1, nbins + 1)
        total_counts = None
        for key in self:
            slice = self[key].extents[t_hat]

            for t in slice:
                df = slice[t]
                extents = df['extent']
                weights = df['n_vortices']

                # extents = cfg_extents[:, 0]
                # weights = cfg_extents[:, 1]
                counts, _ = np.histogram(extents, bins=bins,
                                         weights=weights)
                if total_counts is None:
                    total_counts = counts
                else:
                    total_counts = total_counts + counts

        if normalise:
            total_counts = total_counts / np.sum(total_counts)

        return bins, total_counts


class DistEnsemble(BaseEnsemble):
    def __init__(self, file_list, metadata=None):
        super().__init__(file_list, metadata)
        self.hist = {}
        self.hist_primary = {}
        self.hist_secondary = {}
        # Make distribution histograms for all values of t_hat
        # that are present in all configurations
        for t_hat in self.t_hat_values:
            histograms = self.make_dist_hist(t_hat)
            self.hist[t_hat] = histograms['total']
            self.hist_primary[t_hat] = histograms['primary']
            self.hist_secondary[t_hat] = histograms['secondary']

    def load_configurations(self):
        pattern = re.compile('(\S*)-t(\d)\S*\.hist')
        for filename in self.file_list:
            # Find unique ensembles and group together the different t-hat files
            match = re.match(pattern, filename)
            key = match[1]
            t_hat = int(match[2])
            if self.get(key) is None:
                self[key] = {}

            self[key][t_hat] = filename

        # Store configuration data as a dict of configuration objects
        for key in self:
            self[key] = DistConfiguration(self[key])

    def get_dist_statistics(self, primary_only=False):
        """Calculate ensemble distance statistics

        :param primary_only: (default False) Whether to only consider the primary loop
        :returns result: Dictionary containing the resultant statistics

        """
        columns = [str(i) for i in self.t_hat_values]
        columns.extend(['spatial', 'total'])

        rows = ['dist_lat',
                'dist_lat_err',
                'dist_phys',
                'dist_phys_err',
                'rate_lat',
                'rate_lat_err',
                'rate_phys',
                'rate_phys_err']
        a = self.info.get('a', 1)

        max_dim = np.amax(self.dims)
        dist_samples = -np.ones((self.n_config, self.nd, max_dim))

        # Populate the distance sample array
        start_time = time.perf_counter()
        for icfg, config in enumerate(self.values()):
            if primary_only:
                primary = config.primary
            for t, t_hat in enumerate(self.t_hat_values):
                if primary_only:
                    data = primary[t_hat]
                else:
                    data = config.data[t_hat]
                for ix, slice in enumerate(data.values()):
                    # sample = (slice['size'] * slice['counts']).sum()
                    sample = np.average(slice['size'], weights=slice['counts'])
                    dist_samples[icfg, t, ix] = sample

        end_time = time.perf_counter()
        print(f'Compiling the sample dist array took {end_time-start_time} seconds')

        # Analyse results
        start_time = time.perf_counter()
        dist_val_row, dist_err_row = self.analyse_samples(dist_samples)
        dist_phys_val_row = list(np.array(dist_val_row) / a)
        dist_phys_err_row = list(np.array(dist_err_row) / a)
        end_time = time.perf_counter()
        print(f'Analysing the dist array took {end_time-start_time} seconds\n')

        # Populate the rate sample array
        start_time = time.perf_counter()
        rate_samples = -np.ones((self.n_config, self.nd, max_dim))
        for icfg, config in enumerate(self.values()):
            for t, t_hat in enumerate(self.t_hat_values):
                data = config.data[t_hat]
                for i, ix in enumerate(data):
                    sample = config.estimate_rate(t_hat, ix,
                                                  method='exact',
                                                  primary_only=primary_only)
                    rate_samples[icfg, t, i] = sample
        end_time = time.perf_counter()
        print(f'Compiling the sample rate array took {end_time-start_time} seconds')

        start_time = time.perf_counter()
        rate_val_row, rate_err_row = self.analyse_samples(rate_samples)
        rate_phys_val_row = list(np.array(rate_val_row) / a)
        rate_phys_err_row = list(np.array(rate_err_row) / a)
        end_time = time.perf_counter()
        print(f'Analysing the rate array took {end_time-start_time} seconds\n')

        data = [dist_val_row,
                dist_err_row,
                dist_phys_val_row,
                dist_phys_err_row,
                rate_val_row,
                rate_err_row,
                rate_phys_val_row,
                rate_phys_err_row]

        df = pd.DataFrame(data, columns=columns, index=rows)
        return df

    def make_dist_hist(self, t_hat):
        """Make a histogram of inter-BP distances lengths

        :param t_hat: Which direction is sliced along
        :returns: hist_df, DataFrame of bins, counts and errors

        """
        histograms = {}
        types = ['total', 'primary', 'secondary']
        for type in types:
            merged_df = pd.DataFrame(columns=['size', 'counts'])
            hist_df = pd.DataFrame(columns=['size', 'counts', 'err'])
            for key in self:
                config = self[key]
                primary, secondary = config.primary, config.secondary
                if type == 'total':
                    slices = config.data[t_hat]
                elif type == 'primary':
                    slices = primary[t_hat]
                elif type == 'secondary':
                    slices = secondary[t_hat]

                for t in slices:
                    # Sum the counts by slice and loop
                    summed_df = slices[t].groupby(['size']).counts.sum().reset_index()
                    # Add the results to the total
                    merged_df = pd.concat([merged_df, summed_df]).groupby(['size']).counts.sum()
                    merged_df = merged_df.reset_index()

            hist_df['size'] = merged_df['size']
            hist_df['counts'] = merged_df['counts']

            # Get the error and handle empty bins
            hist_df['err'] = 1 / np.sqrt(merged_df['counts'].replace(0, 1).astype(float))
            histograms[type] = hist_df

        return histograms

    def plot_hist(self, ax, t_hat,  primary_only=False, **kwargs):
        """Plot the histogram dataframe as returned by make_dist_hist

        :param ax: Axes to plot on
        :param t_hat: Which histogram dimension to use
        """

        title = kwargs.pop('title', None)
        cutoff = kwargs.pop('cutoff', np.infty)
        plot_err = kwargs.pop('plot_err', False)
        xlabel = kwargs.pop('xlabel', None)
        ylabel = kwargs.pop('ylabel', None)
        density = kwargs.pop('density', False)

        if primary_only:
            hist_df = self.hist_primary[t_hat]
        else:
            hist_df = self.hist[t_hat]
        plot_df = hist_df.loc[hist_df['size'] <= cutoff]

        bin_min = plot_df['size'].min()
        if cutoff != np.infty:
            bin_max = cutoff
        else:
            bin_max = plot_df['size'].max()
        counts = plot_df['counts']
        err = plot_df['err']

        if density:
            n = counts.sum()
            counts = counts / n
            err = err / n

        if plot_err:
            bin_centres = np.arange(bin_min, bin_max + 1, 1.0)
            width = 0.85
            ax.bar(bin_centres, counts, width=width, yerr=err, alpha=0.7, **kwargs)
        else:
            bins = np.arange(bin_min - 0.5, bin_max + 1.5, 1.0)
            ax.hist(bins[:-1], bins, **kwargs, weights=counts,
                    alpha=0.7, rwidth=0.85)

        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        if not kwargs.get('log'):
            ax.set_ylim(0, None, auto=True)

    def plot_func(self, ax, t_hat, rate, **kwargs):
        """Plot fit to the histogram data with specified rate

        :param ax: Axes on which to plot
        :param t_hat: t_hat to consider
        :param rate: Rate of the geometric distribution

        """

        cutoff = kwargs.pop('cutoff', np.infty)
        fit_colour = kwargs.get('color')

        hist_df = self.hist[t_hat]
        plot_df = hist_df.loc[hist_df['size'] <= cutoff]

        x = np.array(plot_df['size'])
        y = np.array(f_geometric(x, rate))
        ax.plot(x, y, **kwargs)


class GraphEnsemble(BaseEnsemble):

    def __init__(self, file_list, metadata=None):
        super().__init__(file_list, metadata)

    def load_configurations(self):
        pattern = re.compile('(\S*)-t(\d)\S*\.graph')
        for filename in self.file_list:
            # Find unique ensembles and group together the different t-hat files
            match = re.match(pattern, filename)
            key = match[1]
            t_hat = int(match[2])
            if self.get(key) is None:
                self[key] = {}

            self[key][t_hat] = filename

        # Store configuration data as a dict of configuration objects
        for i, key in enumerate(self):
            self[key] = GraphConfiguration(self[key])


# |--------------------- Configuration classes ----------------------|


class BaseConfiguration():
    def __init__(self, files):
        self.files = files
        self.data = {}
        for t_hat in files:
            filename = files[t_hat]
            self.data[t_hat] = self.load_data(filename)

        self.dims = self._set_dims()
        self.split_data()

    def _set_dims(self):
        """Set the lattice dimensions

        :returns: Numpy array of the lattice dimensions
        """
        dims = []
        for t_hat in self.data:
            dims.append(len(self.data[t_hat]))
        return np.array(dims)

    def load_data(self, filename):
        raise NotImplementedError("load_data is not implemented")

    def split_data(self, col='loop'):
        """Split data into primary and secondary loops

        :param col: (default='loop) Column index used to determine
        the primary and secondary loops

        """
        primary = {}
        secondary = {}
        for t_hat in self.data:
            primary[t_hat] = {}
            secondary[t_hat] = {}
            for t in self.data[t_hat]:
                slice = self.data[t_hat][t]
                # Use the most frequent value in col as the indicator of the primary loop
                p_key = slice[col].mode()[0]
                primary[t_hat][t] = slice.loc[slice[col] == p_key]
                secondary[t_hat][t] = slice.loc[slice[col] != p_key]

        self.primary = primary
        self.secondary = secondary

    def slice_dims(self, t_hat):
        """Get the dimensions of a 3D slice

        :param t_hat: Direction being sliced along
        :returns: numpy array of slice dimensions

        """

        return np.array([d for i, d in enumerate(self.dims)
                         if i != t_hat - 1])


class LoopConfiguration(BaseConfiguration):
    def __init__(self, files, extent_files=None):
        super().__init__(files)
        # Load the extents
        if extent_files is not None:
            self.extents = {}
            for t_hat in extent_files:
                filename = extent_files[t_hat]
                self.extents[t_hat] = self.load_extents(filename)

    def load_data(self, filename):
        """Load loops from specified file

        :param filename: File to load
        :returns: Dictionary of loop dataframes, keyed by slice number

        """
        if not filename:
            raise ValueError('No filename provided, exiting')
        # Get first line and number of lines
        with open(filename, "r") as file:
            first_line = file.readline()
            for i, l in enumerate(file):
                pass

        n_lines = i + 2

        match = re.search(r'=\s+(\d+)$', first_line)
        max_rows = int(match.group(1))
        n_slices = n_lines / (max_rows + 1)
        if n_slices.is_integer():
            n_slices = int(n_slices)
        else:
            raise ValueError('Could not determing integer number of slices, aborting')
        n_skip = 1

        slices = {}
        names = ['base_x', 'base_y', 'base_z',
                 'dir_x', 'dir_y', 'dir_z',
                 'type', 'loop']
        for i in range(n_slices):
            slice = pd.read_csv(filename,
                                skiprows=n_skip,
                                nrows=max_rows,
                                sep=r'\s+',
                                names=names,
                                engine='c')
            slice.drop_duplicates(inplace=True)
            slices[i + 1] = slice
            n_skip += max_rows + 1

        return slices

    def load_extents(self, filename):
        if not filename:
            raise ValueError('No filename provided, exiting')

        columns = ['loop', 'slice', 'n_vortices', 'extent']
        fulldata = pd.read_csv(filename,
                               sep=r'\s+',
                               names=columns,
                               engine='c',
                               skiprows=1)
        slices = {}
        x_values = fulldata['slice'].unique()
        for x in x_values:
            slices[x] = fulldata.loc[fulldata['slice'] == x]
            slices[x].drop('slice', axis=1)

        return slices

    def write_loops(self, filename, t_hat, write='all', colour='index'):
        """Output loops in AVS format

        :param filename: Output name
        :param t_hat: Slice dimension
        :param write: (Optional, default='all')
        Choose whether to write 'all', 'primary' or 'secondary' loops
        :param colour: (Optional, default='index')
        Choose whether to colour loops by 'index' or a single colour ('mono')

        """
        primary, secondary = self.primary, self.secondary

        if write == 'all':
            data = self.data[t_hat]
        elif write == 'primary':
            data = primary[t_hat]
        elif write == 'secondary':
            data = secondary[t_hat]
        else:
            raise ValueError(f'Unrecongnised value for write: {write}')

        max_rows = np.amax([len(data[t]) for t in data])
        mode = 'w'

        for t in data:
            header = f'x = {t}, Number of entries = {max_rows}\n'
            slice = data[t].copy()
            if colour == 'mono':
                slice['loop'] = 0
            append_rows = max_rows - slice.shape[0]
            if slice.empty:
                print(f"Slice {t} is empty, skipping")
                continue

            last_row = slice.iloc[-1]
            slice_padded = slice.append([last_row] * append_rows)
            output = slice_padded.to_string(index=False,
                                            header=False,
                                            index_names=False)
            with open(filename, mode) as f:
                f.write(header)
                f.write(output + '\n')
            mode = 'a'


class DistConfiguration(BaseConfiguration):
    def __init__(self, files):
        super().__init__(files)

    def load_data(self, filename):
        names = ['size', 'counts', 'slice', 'loop']
        fulldata = pd.read_csv(filename,
                               sep='\s+',
                               names=names,
                               engine='c')
        slices = {}
        x_values = fulldata['slice'].unique()
        for x in x_values:
            slices[x] = fulldata.loc[fulldata['slice'] == x]
            slices[x].drop('slice', axis=1)

        return slices

    def estimate_rate(self, t_hat, i_slice, min_size=0,
                      method='exact', primary_only=False):
        """Estimate the branching rate on a single 3D slice

        :param t_hat: Dimension playing the role of time
        :param i_slice: Which slice to select
        :param min_size: (default=0) Minimum distance to consider in calculation
        :param method: (default='exact') Estimate the rate via an exact expression or via a fit
        :param primary_only: (default=False) Toggle whether to only consider the primary cluster
        :returns: Estimate of the rate

        """
        if primary_only:
            primary = self.primary
            slices = primary[t_hat]
        else:
            slices = self.data[t_hat]

        slice = slices[i_slice]

        fit_df = slice.loc[slice['size'] > min_size]
        if method == 'exact':
            n = fit_df['counts'].sum()
            mean = np.sum(fit_df['counts'] * fit_df['size']) / n
            rate = 1 / (mean - min_size)
        elif method == 'fit':
            n = slice['counts'].sum()
            x = np.array(fit_df['size'])
            y = np.array(fit_df['counts'] / n)
            popt, _ = curve_fit(f_geometric, x, y, p0=0.1)
            rate = popt[0]
        else:
            raise ValueError(f'Unrecognised method {method}')

        return rate


class GraphConfiguration(BaseConfiguration):
    def __init__(self, files):
        super().__init__(files)

    def load_data(self, filename):
        """Load loops from specified file

        :param filename: File to load
        :returns: Dictionary of loop dataframes, keyed by slice number

        """
        if not filename:
            raise ValueError('No filename provided, exiting')

        fulldata = pd.read_csv(filename,
                               sep=r'\s+',
                               engine='c')

        slices = {}
        x_values = fulldata['slice'].unique()
        for x in x_values:
            slices[x] = fulldata.loc[fulldata['slice'] == x]
            slices[x] = slices[x].drop('slice', axis=1)

        return slices

    def save_graph(self, t_hat, basename):
        """Save a graph using pyvis

        :param t_hat: Which direction is playing the role of time
        :param basename: Base filename.
        Resultant filename is basename + '_t<t_hat>_s<slice>.html'

        """
        primary = self.primary
        for t in primary[t_hat]:
            df = primary[t_hat][t]
            G = nx.from_pandas_edgelist(df,
                                        source='start_id',
                                        target='end_id',
                                        edge_attr='distance',
                                        create_using=nx.MultiDiGraph())
            # Populates the nodes and edges data structures
            nt = Network('1080px', '1080px', directed=True)
            nt.set_edge_smooth('dynamic')
            # nt.toggle_physics(False)
            nt.from_nx(G)
            nt.show_buttons(filter_=['physics'])

            nt = self._style_network(nt)

            filename = basename + f'_t{t_hat}_s{t}.html'
            print(f'Saving {filename}')
            nt.save_graph(filename)

    def _style_network(self, nt):
        connections = {}
        for edge in nt.get_edges():
            # self_connect = (edge['from'] == edge['to'])
            if edge['from'] not in connections:
                connections[edge['from']] = 1
            else:
                connections[edge['from']] += 1

            # if not self_connect:
            if edge['to'] not in connections:
                connections[edge['to']] = 1
            else:
                connections[edge['to']] += 1

        for node_id in nt.get_nodes():
            node = nt.get_node(node_id)
            node['label'] = ''
            node['borderWidth'] = 1.5
            if connections[node_id] == 3:
                node['color'] = {'background': 'rgba(0, 128, 128, 0.5)',
                                 'border': 'rgba(0, 128, 128, 1)'}
            elif connections[node_id] == 4:
                node['color'] = {'background': 'rgba(255, 165, 0, 0.5)',
                                 'border': 'rgba(255, 165, 0, 1)'}
            elif connections[node_id] == 5:
                node['color'] = 'green'
            elif connections[node_id] == 6:
                node['color'] = 'yellow'

        for edge in nt.get_edges():
            edge['color'] = 'black'

        return nt

# |------------------------- Fit functions --------------------------|


def f_geometric(t, p):
    """Geometric distribution"""
    return p * (1 - p) ** (t - 1)


def jackknife(arr):
    """Calculate the jackknife mean and err of an input array

    :param arr: Array of samples
    :returns: mean, err as determined from the single-elimination jackknife

    """
    z = np.array(arr)
    n = len(arr)
    z_mean = np.mean(z)
    zj = (n * z_mean - z) / (n - 1)
    err = np.sqrt((n - 1) * np.mean((zj - np.mean(zj)) ** 2))
    return z_mean, err
