import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import yaml
import os

mpl.rc('text', usetex=True)
mpl.rc('font', family='serif')
mpl.rcParams['axes.unicode_minus'] = False
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}')

dataDir = "/g/data/e31/jb3708/data/PlottingCoordinates/Coordinates/UT-MCG-VO/32x64_B1900_Kud01378100_Ks01364000/BPDensity"

fNames = [os.path.join(dataDir, "BPDists{}.dat".format(i)) for i in range(1,5)]

dims = [32, 32, 32, 64]

fig, axes = plt.subplots(nrows=2, ncols=2, constrained_layout=True)

for i, (f, ax) in enumerate(zip(fNames, axes.flat)):
    localDims = [dims[j] for j in range(4) if j != i]
    maxDist = np.linalg.norm(localDims)
    bins = np.arange(0, np.ceil(maxDist + 1), 1.0)
    data = np.loadtxt(f, max_rows=100000)
    ax.hist(data, bins=bins)
    ax.set_title(r"$\mu = {}$".format(i + 1))
    ax.set_ylabel("Separation")
    ax.set_xlabel("Counts")
    print("Finished mu = {}".format(i+1))

plt.savefig("BPDists.pdf", format="pdf")
plt.close('all')
