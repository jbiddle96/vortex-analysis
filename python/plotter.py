import functools
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

mpl.rc('text', usetex=True)
mpl.rc('font', family='serif', size=16)
mpl.rcParams['axes.unicode_minus'] = False
mpl.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')


class Plotter:
    def __init__(self, columns, basename, format='pdf'):
        """Define a class to store histogram data to facilitate simultaneaous plotting

        :param columns: List of dataframe columns
        :param basename: Base filename string.
        Keys, suffixes and extensions will be added as necessary
        :param format: Output file format (default 'pdf')

        """
        self.basename = basename
        self.format = format
        self.columns = columns
        self.columns += ['order', 'data']
        self.datasets = pd.DataFrame(columns=self.columns)

    def add_data(self, keys, weights, bins, label=None, alpha=1, order=0):
        """Add a dataset to the plotter
        """

        data = HistogramData(weights, bins, label=label, alpha=alpha)
        row = keys.copy()
        row['order'] = order
        row['data'] = data
        self.datasets = self.datasets.append(row, ignore_index=True)

    def plot_histogram(self, keys=None, groupby=None, group_order=None,
                       split=False, shape=None, suffix='', dir='', **kwargs):
        """Plot histograms based on the stored data.
        If keys are not supplied, all data is plotted.

        :param keys: Dictionary of column_name: value pairs that are used to cut the datasets.
        (default None)
        :param groupby: Value or list of values on which to group datasets. (default None)
        :param group_order: Order in which the groups are to appear in plots. (default None)
        :param split: Toggle whether to split groups across separate files (True)
        or different axes on the same figure (False). (default False)
        :param shape: Specify the shape of the output graph as (nrows, ncols).
        nrows * ncols must equal the number of groups as constructed by 'groupby'.
        If not provided, nrows = # of unique groups and ncols=1. (default None)
        :param suffix: Suffix to append to the base filename. (default '')

        """

        if keys is not None:
            plot_df = self.datasets

            def get_mask(x, a):
                return x & (plot_df[a] == keys[a])

            mask = functools.reduce(get_mask,
                                    keys, len(plot_df) * [True])
            plot_df = self.datasets.loc[mask]
        else:
            plot_df = self.datasets
            keys = {}

        if plot_df.empty:
            raise ValueError('''Plotting dataframe is empty.
            Perhaps an invalid key was supplied?''')

        if groupby is not None:
            groups = plot_df.groupby(groupby)
            groups = {key: val for (key, val) in groups}
        else:
            groups = {'': plot_df}

        if group_order is None:
            group_order = list(groups.keys())

        xlabel = kwargs.pop('xlabel', None)
        ylabel = kwargs.pop('ylabel', None)
        sharex = kwargs.pop('sharex', False)
        sharey = kwargs.pop('sharey', False)
        xmin = kwargs.pop('xmin', None)
        xmax = kwargs.pop('xmax', None)
        ymin = kwargs.pop('ymin', None)
        ymax = kwargs.pop('ymax', None)
        kw_alpha = kwargs.pop('alpha', None)

        figs, axes, fig_names = self._figure_layout(split,
                                                    group_order,
                                                    keys,
                                                    shape,
                                                    sharex,
                                                    sharey,
                                                    suffix)
        # Loop over unique groups
        for group, ax in zip(group_order, axes):
            df = groups[group]
            sort_df = df.sort_values('order')
            orders = sort_df.groupby('order')
            # Loop over unique orders
            for (order, order_df) in orders:
                order_weights = []
                labels = []
                alpha_values = []
                for hist_data in order_df['data'].values:
                    order_weights.append(hist_data.weights)
                    # Bins must be the same for all datasets in group
                    bins = hist_data.bins
                    labels.append(hist_data.label)
                    alpha_values.append(hist_data.alpha)

                x = [bins[:-1] for i in range(len(order_weights))]
                order_weights = np.squeeze(order_weights).T
                labels = np.squeeze(labels)
                x = np.squeeze(x).T

                alpha = np.amin(alpha_values)
                # Overwrite alpha if one is supplied in kwargs
                if kw_alpha is not None:
                    alpha = kw_alpha

                ax.hist(x,
                        bins,
                        weights=order_weights,
                        label=labels,
                        alpha=alpha,
                        **kwargs)
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)

        if split:
            for ax in axes:
                ax.legend()
        else:
            axes[0].legend()

        for ax in axes:
            ax.set_ylim(ymin, ymax)
            ax.set_xlim(xmin, xmax)

        for fig, fig_name in zip(figs, fig_names):
            print(f"Saving {fig_name}")
            out_name = os.path.join(dir, fig_name)
            fig.savefig(out_name, bbox_inches="tight")
            plt.close(fig)

    def _figure_layout(self, split, group_order, keys,
                       shape, sharex, sharey, suffix):
        """Construct a figure layout and build filenames
        """
        ngroups = len(group_order)
        fig_names = []
        figs = []

        if shape is not None:
            if np.prod(shape) != ngroups:
                raise ValueError('Product of shape parameter must be'
                                 + ' the same as the number of groups')
            nrows = shape[0]
            ncols = shape[1]
        else:
            nrows = ngroups
            ncols = 1

        if split:
            axes = []
            for group in group_order:
                fig, ax = plt.subplots(constrained_layout=True,
                                       sharex=sharex,
                                       sharey=sharey)
                figs.append(fig)
                axes.append(ax)
                group_str = functools.reduce(lambda x, a: x + '_' + a,
                                             group, '')
                key_str = functools.reduce(lambda x, a: x + '_' + keys[a],
                                           keys, '')
                base_str = self.basename + f'{group_str}{key_str}{suffix}'
                filename = f'{base_str}.{self.format}'
                fig_names.append(filename)
        else:
            fig, axes = plt.subplots(nrows=nrows,
                                     ncols=ncols,
                                     constrained_layout=True,
                                     sharex=sharex,
                                     sharey=sharey)
            figs = [fig]
            key_str = functools.reduce(lambda x, a: x + '_' + keys[a],
                                       keys, '')
            base_str = self.basename + f'{key_str}{suffix}'
            filename = f'{base_str}.{self.format}'
            fig_names.append(filename)

        axes = np.array([axes]).flatten()

        return figs, axes, fig_names


class HistogramData:
    def __init__(self, weights, bins, label=None, alpha=1):
        """Simple class to store histogram data

        :param weights: Array of weights to use for each bin
        :param bins: Array of bin edges
        :param label: Legend label, if None will default to key. (default None)

        """
        self.bins = bins
        self.weights = weights
        self.label = label
        self.alpha = alpha

        if len(bins) - 1 != len(weights):
            raise ValueError(f"""bins must have length 1 greater than weights
            len(bins) = {len(bins)}
            len(weights) = {len(weights)}""")
