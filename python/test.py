import pandas as pd
import csv
import sys
import time


filename = sys.argv[1]

start_time = time.perf_counter()
with open(filename) as f:
    file = csv.DictReader(f)
    for item in file:
        print(item)

end_time = time.perf_counter()

print(f'Loading with csv.DictReader took {end_time-start_time} seconds')

start_time = time.perf_counter()
df = pd.read_csv(filename,
                 sep=r'\s+')
end_time = time.perf_counter()

print(f'Loading with pandas took {end_time-start_time} seconds')
