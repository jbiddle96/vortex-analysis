import sys
import numpy as np
import os

# Script to pad AVS files with an equal number of entries per section
# Section must start with "x"
filename = sys.argv[1]
base = os.path.basename(filename)
print("Inserting lines into", base, "\n")
f = open(filename, "r")
contents = f.readlines()
f.close()

section_start = np.array([]).astype(int)
for line_number, line in enumerate(contents, 1):
    thisline = line.strip()
    if (thisline[0]=="x"):
        section_start = np.append(section_start,line_number)

section_start = np.append(section_start,len(contents)+1)

section_length = np.array([section_start[i+1] - section_start[i]\
                           for i in range(0,len(section_start)-1)]) - 1

max_section_size = np.amax(section_length)

contents = [line.rstrip() + "  entries = " + str(max_section_size) + "\n"\
            if line.lstrip().startswith("x") else line for line in contents]

line=0
for sec_length in section_length:
    line += sec_length
    numlines = max_section_size - sec_length
    for i in range(numlines):
        contents.insert(line,str(contents[line]))
    line += numlines + 1

f = open(filename, "w")
contents = "".join(contents)
f.write(contents)
f.close()
