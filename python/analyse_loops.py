"""
Analyse vortex loops to identify configurations of interest
and determine loop statistics.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re
import os
import importlib
import plotter as pl
from cache import cache
from tabulate import tabulate
import vis_helper as vis
import lattice_types as lt
import make_table as mt
importlib.reload(vis)
importlib.reload(lt)
importlib.reload(pl)
importlib.reload(mt)


def plot_histogram(ax, counts, bins, **kwargs):
    title = kwargs.pop('title', None)
    xlabel = kwargs.pop('xlabel', 'Loop size')
    ylabel = kwargs.pop('ylabel', 'Ensemble average counts')
    alpha = kwargs.pop('alpha', 1)
    normalise = kwargs.pop('normalise', False)

    if normalise:
        counts = counts / np.sum(counts)

    ax.hist(bins[:-1], bins, weights=counts, histtype='step', **kwargs)
    # y, binEdges = np.histogram(data, bins=10)
    # bincenters = 0.5 * (bins[1:] + bins[:-1])
    # width = 0.85
    # ax.bar(bincenters, counts, width=width, yerr=err, **kwargs)

    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)


if __name__ == '__main__':

    config, general = vis.load_config()
    clean = general.get('clean', False)
    meta_file = general.get('meta_file', None)
    max_size = general.get('max_size', np.infty)
    min_size = general.get('min_size', 0)

    cache_dir = vis.assign_dir('./cache', clean)

    default_width = 6.4
    default_height = 4.8
    combined_dir = vis.assign_dir('combined_loop_analysis', clean)

    cluster_extents = general.get('cluster_extents', True)

    if cluster_extents:
        extent_plotter = pl.Plotter(['t_hat', 'cfg'], 'cluster_extent')
    loop_plotter = pl.Plotter(['t_hat', 'cfg', 'norm'], 'loophist')

    stats = {}
    for key in config:
        cfg = config[key]
        file_list = cfg['file_list']

        type = cfg.get('type', key)
        metadata = vis.load_metadata(meta_file, type)

        name = cfg.get('name', key)
        output_dir = f'{name}_loop_analysis'
        output_dir = vis.assign_dir(output_dir, clean)
        cfg['output_dir'] = output_dir
        variant_dir = os.path.join(output_dir, 'loop_variants')
        variant_dir = vis.assign_dir(variant_dir, clean)

        label = cfg.get('label', key)
        alpha = cfg.get('alpha', 1)
        order = cfg.get('order', 0)

        # Load the ensemble
        ensemble = lt.LoopEnsemble(file_list, metadata, load_extents=cluster_extents)

        # Calculate cluster statistics
        stats[key] = ensemble.get_cluster_statistics()
        filename = os.path.join(cache_dir, f'stats.pickle')
        cache(stats, cache_name=filename)
        # stats[key].to_csv(filename)
        formatted_table = tabulate(stats[key],
                                   headers='keys',
                                   tablefmt='psql')
        filename = os.path.join(output_dir, f'stats.txt')
        with open(filename, 'w') as f:
            f.write(formatted_table)

        # Find large secondary loops
        size = cfg.get('find_size', None)
        if size is not None:
            logfile = os.path.join(output_dir, f'large_loops_{size}.log')
            ensemble.find_large_secondary_loops(size, logfile)

        # Output the secondary loops
        if cfg.get('output_secondary', True):
            for cfg_key in ensemble:
                configuration = ensemble[cfg_key]
                for t_hat in ensemble.t_hat_values:
                    basename = os.path.basename(cfg_key)
                    filename = f'{basename}-t{t_hat}_secondary.loops'
                    filename = os.path.join(variant_dir, filename)
                    print(f"Saving {filename}")
                    configuration.write_loops(filename, t_hat,
                                              write='secondary')

                    filename = f'{basename}-t{t_hat}_secondary_mono.loops'
                    filename = os.path.join(variant_dir, filename)
                    print(f"Saving {filename}")
                    configuration.write_loops(filename, t_hat,
                                              write='secondary',
                                              colour='mono')

        # |---------------------------- Plotting ----------------------------|

        # Plot cluster extents
        if cluster_extents:
            for t_hat in ensemble.t_hat_values:
                nbins = 10
                bins, counts = ensemble.bin_cluster_extents(t_hat, nbins, normalise=True)
                keys = {'t_hat': f't{t_hat}', 'cfg': key}
                extent_plotter.add_data(keys,
                                        counts,
                                        bins,
                                        label=label,
                                        alpha=alpha,
                                        order=order)

        for t_hat in ensemble.t_hat_values:
            # No norm
            counts, err, bins = ensemble.make_loop_hist(t_hat,
                                                        min_size=min_size,
                                                        max_size=max_size)
            keys = {'t_hat': f't{t_hat}', 'cfg': key, 'norm': 'none'}
            loop_plotter.add_data(keys,
                                  counts,
                                  bins,
                                  label=label,
                                  alpha=alpha,
                                  order=order)

            # Volume norm
            counts, err, bins = ensemble.make_loop_hist(t_hat,
                                                        normalise='physical',
                                                        min_size=min_size,
                                                        max_size=max_size)
            keys['norm'] = 'physical'
            loop_plotter.add_data(keys,
                                  counts,
                                  bins,
                                  label=label,
                                  alpha=alpha,
                                  order=order)

            # Vortex norm
            counts, err, bins = ensemble.make_loop_hist(t_hat,
                                                        normalise='vortex',
                                                        min_size=min_size,
                                                        max_size=max_size)
            keys['norm'] = 'vortex'
            loop_plotter.add_data(keys,
                                  counts,
                                  bins,
                                  label=label,
                                  alpha=alpha,
                                  order=order)

    # Save cluster extent plots
    if cluster_extents:
        extent_plotter.plot_histogram(groupby='t_hat', split=True,
                                      xlabel='Cluster extent',
                                      ylabel='Proportion of vortices',
                                      log=True,
                                      rwidth=0.85,
                                      dir=combined_dir)
        for key in config:
            cfg = config[key]
            extent_plotter.plot_histogram(keys={'cfg': key},
                                          groupby='t_hat', split=True,
                                          xlabel='Cluster extent',
                                          ylabel='Proportion of vortices',
                                          log=True,
                                          rwidth=0.85,
                                          alpha=1,
                                          dir=cfg['output_dir'])

    # Save loop extent plots
    loop_plotter.plot_histogram(groupby=['t_hat', 'norm'], split=True,
                                xlabel='Cluster size',
                                ylabel='Proportion of clusters',
                                rwidth=0.85,
                                dir=combined_dir)
    loop_plotter.plot_histogram(groupby=['t_hat', 'norm'], split=True,
                                xlabel='Cluster size',
                                ylabel='Proportion of clusters',
                                log=True,
                                suffix="_log",
                                rwidth=0.85,
                                dir=combined_dir)

    for key in config:
        cfg = config[key]
        loop_plotter.plot_histogram(keys={'cfg': key},
                                    groupby=['t_hat', 'norm'],
                                    split=True,
                                    xlabel='Loop size',
                                    ylabel='Proportion of clusters',
                                    rwidth=0.85,
                                    alpha=1,
                                    dir=cfg['output_dir'])

        loop_plotter.plot_histogram(keys={'cfg': key},
                                    groupby=['t_hat', 'norm'],
                                    split=True,
                                    xlabel='Loop size',
                                    ylabel='Proportion of clusters',
                                    log=True,
                                    suffix="_log",
                                    rwidth=0.85,
                                    alpha=1,
                                    dir=cfg['output_dir'])
