import numpy as np
import os
import yaml
import vis_helper as vis
import importlib
from cache import load_cache
import pandas as pd
importlib.reload(vis)


def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)


def load_stats(config):
    """Load the Fortran output table

    :param config: Configuration dictionary
    :returns: Dictionary of dataframes matching the keys in config

    """
    data = {}
    for key, cfg in config.items():
        stat_file = os.path.join(cfg['dir'], 'vortex_statistics.csv')
        df = pd.read_csv(stat_file, sep=',\s*', engine='python')
        # Strip string columns
        df_obj = df.select_dtypes(['object'])
        df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip())

        # Strip columns and index
        columns = [c.strip() for c in df.columns]
        df.columns = columns
        index = [i.strip() for i in df.index]
        df.index = index

        # Split into err and value dataframes
        df, df_err = split_dataframe(df)
        data[key] = df, df_err

    return data


def split_dataframe(df):
    df_err = df.loc[[s.endswith('_err') for s in df.index]]
    df_val = df.loc[[not s.endswith('_err') for s in df.index]]

    return df_val, df_err


def get_reduced_table(data: dict,
                      config: dict,
                      column: str,
                      indices: list,
                      column_labels: dict=None
                      ) -> (pd.DataFrame, pd.DataFrame):
    """Transpose the data dictionary to produce a single table

    Table has columns for the ensemble and the specified indices.
    Each row takes values from the specified column.

    :param data: Dictionary of stats dataframes for each ensemble.
    :param config: Configuration info.
    :param column: Column to access for row values.
    :param indices: Indices to convert to rows.
    :param column_labels: Dictiionary of labels to lookup for the columns.
    :param pd.DataFrame:
    :returns: (df, df_err), the value and error dataframes.

    """
    if column_labels is None:
        column_labels = {}

    rows = []
    err_rows = []
    for key, (df, df_err) in data.items():
        cfg = config[key]
        row = [cfg.get('label', key)]
        row = row + [df.at[i, column] for i in indices]

        err_row = [cfg.get('label', key)]
        err_row = err_row + [df_err.at[i + '_err', column] for i in indices]

        rows.append(row)
        err_rows.append(err_row)

    columns = ['Ensemble'] + [column_labels.get(i, i) for i in indices]

    df = pd.DataFrame(rows, columns=columns)
    df = df.set_index('Ensemble')
    df_err = pd.DataFrame(err_rows, columns=columns)
    df_err = df_err.set_index('Ensemble')
    return df, df_err


def make_latex_table(df, df_err,
                     header_labels=None,
                     row_labels=None,
                     rule_str=r'\midrule',
                     nan_str='-'):
    """Generate a latex table from the contents of df

    :param df: Dataframe to tabulate
    :param df_err: Dataframe of errors. Must be of same shape as df
    :param header_labels: (default None) Dictionary of header names to display.
    Keys must be columns in df
    :param row_labels: (default None) Dictionary of row names to display.
    Keys must be in the index of df
    :param rule_str: String to use as the rule line between the table header and content.
    :param nan_str: (default '-') String to insert in place of NaN values
    :returns: List of strings containing each row of the table

    """
    if header_labels is None:
        header_labels = {}
    if row_labels is None:
        row_labels = {}

    vectoriseformat = np.vectorize(vis.format_error)
    values = vectoriseformat(df, df_err)
    df_table = pd.DataFrame(values, columns=df.columns, index=df.index)

    header_names = [header_labels.get(h, h) for h in df_table.columns]

    # Merge the header into one string
    header = header_names
    index_name = df.index.name if df.index.name is not None else ''
    header = f'{index_name} & ' + ' & '.join(header) + ' \\\\'

    table = [header, rule_str]
    n_rows = len(df_table.index)
    for index, row in df_table.iterrows():
        index_str = row_labels.get(index, index)
        row_str_list = [index_str] + row.tolist()
        row_string = ' & '.join(row_str_list) + ' \\\\'
        table.append(row_string)

    # Remove trailing '\\' from last row
    table[-1] = rreplace(table[-1], r'\\', '', 1)

    return table


def make_multitable(tables: dict,
                    rule_str: str = r'\midrule',
                    table_labels: dict = None) -> list:

    if table_labels is None:
        table_labels = {}

    multitable = []

    for key, table in tables.items():
        # If multitable is empty, add the header row
        if not multitable:
            multitable.append(table[0])

        multitable.append(rule_str)
        name = table_labels.get(key, key)
        # Add the multitable header
        sec_str = r'\multicolumn{{1}}{{l}}{{{}}}\\'.format(name)
        multitable.append(sec_str)
        # Insert the table contents
        multitable.extend(table[1:])
        # Re-insert the trailing '\\'
        multitable[-1] = multitable[-1] + r'\\'

    # Remove trailing '\\' from last row
    multitable[-1] = rreplace(multitable[-1], r'\\', '', 1)

    return multitable


def write_table(filename, table_data):
    """Output the table to a tex file
    """

    with open(filename, 'w+') as f:
        for row in table_data:
            f.write(row + '\n')

    print(f'Saved {filename}')


if __name__ == '__main__':
    config, general = vis.load_config()
    clean = general.get('clean', False)
    # tex_dir = vis.assign_dir('./tex', clean)

    row_labels = general.get('row_labels')
    header_labels = general.get('header_labels')
    table_labels = {key: cfg.get('label', key) for key, cfg in config.items()}
    graph_dir = general.get('graph_dir')
    loop_dir = general.get('loop_dir')

    data = load_stats(config)

    # Load and merge the graph analysis dataframe if it is present
    if graph_dir:
        cache_name = os.path.join(graph_dir, 'cache', 'stats_all.pickle')
        graph_data = load_cache(cache_name)
        graph_data = {key: split_dataframe(graph_data[key]) for key in graph_data}
        common_keys = set(graph_data.keys()).union(set(data.keys()))
        for key in common_keys:
            df_g, df_g_err = graph_data[key]
            df, df_err = data[key]
            data[key] = pd.concat([df, df_g]), pd.concat([df_err, df_g_err])

    # Load and merge the loop analysis dataframe if it is present
    if loop_dir:
        cache_name = os.path.join(loop_dir, 'cache', 'stats.pickle')
        loop_data = load_cache(cache_name)
        loop_data = {key: split_dataframe(loop_data[key]) for key in loop_data}
        common_keys = set(loop_data.keys()).union(set(data.keys()))
        for key in common_keys:
            df_g, df_g_err = loop_data[key]
            df, df_err = data[key]
            data[key] = pd.concat([df, df_g]), pd.concat([df_err, df_g_err])

    # Make table of all statistics
    tables = {}
    for key, (df, df_err) in data.items():
        tables[key] = make_latex_table(df, df_err,
                                       row_labels=row_labels,
                                       header_labels=header_labels)
        filename = f'{key}_table.tex'
        write_table(filename, tables[key])

    multitable = make_multitable(tables, table_labels=table_labels)
    filename = 'multi_table.tex'
    write_table(filename, multitable)

    # Make BP table
    tables_BP = {}
    rows = ['rho_BP_lat', 'rho_BP_phys', 'prob_BP_lat', 'prob_BP_phys']
    err_rows = [r + '_err' for r in rows]
    columns = ['4', 'spatial', 'total']
    for key, (df, df_err) in data.items():
        df_BP = df.loc[rows, columns]
        df_BP_err = df_err.loc[err_rows, columns]
        tables_BP[key] = make_latex_table(df_BP, df_BP_err,
                                          row_labels=row_labels,
                                          header_labels=header_labels)
        filename = f'{key}_BP_table.tex'
        write_table(filename, tables_BP[key])

    multitable = make_multitable(tables_BP, table_labels=table_labels)
    filename = 'multi_BP_table.tex'
    write_table(filename, multitable)

    # Make vortex table
    tables_vor = {}
    rows = ['rho_vor_lat', 'rho_vor_phys']
    err_rows = [r + '_err' for r in rows]
    columns = ['4', 'spatial', 'total']
    for key, (df, df_err) in data.items():
        df_vor = df.loc[rows, columns]
        df_vor_err = df_err.loc[err_rows, columns]
        tables_vor[key] = make_latex_table(df_vor, df_vor_err,
                                          row_labels=row_labels,
                                          header_labels=header_labels)
        filename = f'{key}_vortex_table.tex'
        write_table(filename, tables_vor[key])

    multitable = make_multitable(tables_vor, table_labels=table_labels)
    filename = 'multi_vortex_table.tex'
    write_table(filename, multitable)

    # Make loop table
    if loop_dir:
        tables_loop = {}
        rows = ['N_total', 'N_primary', 'N_secondary']
        err_rows = [r + '_err' for r in rows]
        columns = ['4', 'spatial']
        print(df)
        print(df_err)
        for key, (df, df_err) in data.items():
            df_loop = df.loc[rows, columns]
            df_loop_err = df_err.loc[err_rows, columns]
            tables_loop[key] = make_latex_table(df_loop, df_loop_err,
                                                header_labels=header_labels,
                                                row_labels=row_labels)
            filename = f'{key}_loop_table.tex'
            write_table(filename, tables_loop[key])

        multitable = make_multitable(tables_loop, table_labels=table_labels)
        filename = 'multi_loop_table.tex'
        write_table(filename, multitable)

    if graph_dir:
        # Make reduced rate table
        # Use row labels as column labels as data is being transposed
        indices = ['prob_BP_lat', 'prob_BP_phys', 'rate_lat', 'rate_phys']
        df_rate, df_rate_err = get_reduced_table(data,
                                                 config,
                                                 'total',
                                                 indices,
                                                 column_labels=row_labels)
        table = make_latex_table(df_rate, df_rate_err)
        filename = f'rate_table.tex'
        write_table(filename, table)


    # Make reduced vortex table
    indices = ['rho_vor_lat', 'rho_vor_phys', 'rho_BP_lat', 'rho_BP_phys']
    df_vor, df_vor_err = get_reduced_table(data,
                                           config,
                                           'total',
                                           indices,
                                           column_labels=row_labels)
    table = make_latex_table(df_vor, df_vor_err)
    filename = f'vortex_table.tex'
    write_table(filename, table)
