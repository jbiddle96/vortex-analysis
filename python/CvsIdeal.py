#!/apps/python3/3.7.4/bin/python3
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import importlib
from matplotlib.legend_handler import HandlerTuple, HandlerBase
from matplotlib.text import Text
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import os, sys
import vis_helper as vis
importlib.reload(vis)

class TextHandler(HandlerBase):
    def create_artists(self, legend, txt,
                   xdescent, ydescent, width, height, fontsize, trans):
        tx = Text(width/2.,height/2, txt, fontsize=fontsize,
                  ha="center", va="center", fontweight="bold")
        return [tx]

def loadData(datasets):
    dataDict = {}
    for key in datasets:
        data = np.loadtxt(datasets[key], skiprows=1)
        C = data[:,1]
        C_Ideal = data[:,2]
        ConCI = C/C_Ideal
        Cbar = np.mean(ConCI)
        C_std = np.std(ConCI)
        dataDict[key] = [Cbar, C_std]
    return dataDict

def genPlot(dataDict, axes, makeLegend = False, title = None):
    # plt.figure(fig.number)
    legend_cols = []
    ylist = []
    left, right = -0.5, 2.5
    axes.set_xlim(left, right)
    
    for i,key in enumerate(dataDict):
        Cbar = dataDict[key][0]
        Error = dataDict[key][1]
        print("Cbar for", dir, key, " = ", Cbar, "+-", Error)
        
        ylist.extend([Cbar, 1.0])
        
        colour = 'C'+str(i)
        axes.get_xaxis().set_ticks([])
        dots, = axes.plot(i, Cbar, marker = 'o', markersize = 3,
                          linewidth=0, label=key, color = colour)
        axes.errorbar(i, Cbar, yerr = Error, capsize=2, elinewidth=0.5,
                      marker = 'o', markersize = 4, linewidth= 0,
                      label=key, color = colour)
        
        ACline = axes.hlines(1,left, right, color = 'black',
                             linestyles=':')
        
        legend_cols.extend([key, dots])
        
        ncols = i+1
 
    Cbar_all = [dataDict[key][0] for key in dataDict.keys()]
    ymax = max(0.2, max(Cbar_all)+0.05)
    axes.set_ylim(0, ymax)
    
    legend_labels = ["",r"$\overline{C/C_\text{ideal}}$"] + 6*[""]
    if(makeLegend):
        axes.legend(legend_cols, legend_labels, ncol = ncols,
                    handler_map={str: TextHandler()}, markerfirst=False,
                    bbox_to_anchor=(0.5, 0.1), bbox_transform=plt.gcf().transFigure,
                    loc='center') 
    
    if(title):
        axes.set_title(title)


if __name__ == '__main__':
    mpl.rc('text', usetex=True)
    mpl.rc('font', family='serif')
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rc('text.latex', preamble=r'\usepackage{amsmath}')

    config, general = vis.load_config()
    dirs = [cfg['dir'] for cfg in config.values()]
    labels = [cfg.get('label', i) for i, cfg in enumerate(config.values())]
    titles = [cfg.get('title', i) for i, cfg in enumerate(config.values())]
    indep_y_axes = [cfg.get('own_y_axis', False) for i, cfg in enumerate(config.values())]

    # Determine the number of plots in each gridspec group
    gridspec_widths = []
    count = 0
    for val in indep_y_axes:
        if val:
            gridspec_widths.extend([count, 1])
            count = 0
        else:
            count += 1

    datasets = {"V":'Vortex-TopQCorrelation.dat',
                "SP":'SP-TopQCorrelation.dat',
                "BP":'BP-TopQCorrelation.dat'}

    current_datasets = {}

    # Make the figure. Height is 1/4 of A4
    width = max(1.8 * len(config), 3)
    fig = plt.figure(figsize=(width, 11.69/4))
    n_plots = len(gridspec_widths)
    # Make an outer gridspec to group plots based on sharing the y axis
    outer = gridspec.GridSpec(1, n_plots, width_ratios=gridspec_widths,
                              wspace = n_plots * 0.1, figure = fig)

    # Divide the grouped specs into individual axes
    specs = []
    for width, gs in zip(gridspec_widths, outer):
        this_gs = gridspec.GridSpecFromSubplotSpec(1, width, subplot_spec=gs, wspace = 0.2)
        specs.extend(this_gs)

    # Bundle all plots into an array of axes
    # These can now be treated as regular axes objects, but with fixed spacing
    axes = []
    for cell in specs:
        axes.append(plt.subplot(cell))

    # Iterate through directories and read date
    for i, dir in enumerate(dirs):
        print("Entering directory:", dir)
        for key in datasets:
            current_datasets[key] = os.path.join(dir, datasets[key])

        dataDict = loadData(current_datasets)

        ax = axes[i]
        if (i==len(dirs) - 1):
            genPlot(dataDict, axes=ax, title = titles[i], makeLegend = True)
        else:
            genPlot(dataDict, axes=ax, title = titles[i])
        print("\n")

    axes[0].set_ylabel(r"$\overline{C/C_\text{ideal}}$")

    # Share y axis labels for groups of plots
    start_idx = 0
    for width in gridspec_widths:
        main_ax = axes[start_idx]
        for ax in axes[start_idx + 1: start_idx + width]:
            ax.get_shared_y_axes().join(ax, main_ax)
            ax.set_yticklabels([])
        start_idx += width

    # Add x axis text
    # and shrink plot height to allow room for the legend
    for ax,label in zip(axes, labels):
        ax.text(0.5,-0.15, label,
               horizontalalignment='center', transform=ax.transAxes)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.3,
                         box.width, box.height * 0.7])

    fname = './CvsIdeal_plotmulti.pdf'
    fig.savefig(fname , papertype='a4')
    print("Generated plot: ", fname, "\n")
