"""Plot graphs as html files"""
import matplotlib.pyplot as plt
import os
import re
import importlib
import vis_helper as vis
import lattice_types as lt
importlib.reload(vis)
importlib.reload(lt)


if __name__ == '__main__':
    config, general = vis.load_config()
    clean = general.get('clean', False)
    meta_file = general.get('meta_file', None)
    default_width = 6.4
    default_height = 4.8
    ncols, nrows = 2, 3

    for key in config:
        cfg = config[key]
        plot_dir = cfg.get('plot_dir', '')
        plot_files = cfg.get('plot_files')
        type = cfg.get('type', key)
        metadata = vis.load_metadata(meta_file, type)

        # Make output dirs
        name = cfg.get('name', key)
        output_dir = os.path.join(f'./{name}_graph_analysis', 'network_plots')
        output_dir = vis.assign_dir(output_dir, clean)

        if plot_files is None:
            continue

        filenames = [os.path.join(plot_dir, f) for f in plot_files]

        graph_ensemble = lt.GraphEnsemble(filenames,
                                          metadata=metadata)

        # Plot all desired configurations for all t_hat values
        for key in graph_ensemble:
            print(key)
            graph_config = graph_ensemble[key]
            for t_hat in graph_config.data:
                basename = os.path.basename(key)
                out_name = os.path.join(output_dir, basename)
                # print(f'Saving {out_name}')
                graph_config.save_graph(t_hat, out_name)
