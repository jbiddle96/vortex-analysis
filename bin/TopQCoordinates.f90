program TopQCoordinates

  use Kinds
  use FileIO
  use AVSCoordinates
  use TopQAnalysis
  use AnalyseLinks
  use ArrayOps
  implicit none

  real(dp), dimension(:, :, :, :), allocatable :: topQ, dyntopQ
  integer :: t_hat

  ! File I/O variables
  character(len=256) :: TOPQDIR, TOPQOUTDIR
  logical :: topqscale

  write(*,*) "What is the input file for the topological charge?"
  read(*,*) TOPQDIR

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt
  dims = [nx, ny, nz, nt]

  write(*,*) "Which direction is playing the role of time?"
  read(*,*) t_hat
  write(*,*) t_hat

  write(*,*) "What is the output directory for the topological charge coordinates?"
  read(*,*) TOPQOUTDIR
  write(*,*) TOPQOUTDIR

  write(*,*) "Should the topological charge be dynamically scaled?"
  read(*,*) topqscale
  write(*,*) topqscale

  call ChargeCoords(topQ, dyntopQ, trim(TOPQDIR), trim(TOPQOUTDIR), topqscale, t_hat)
  write(*,*) "Finished generating topological charge coordinates for ", trim(TOPQDIR)
  write(*,*)

end program TopQCoordinates
