! Generates correlation data for vortices vs topQ, branching points vs topQ and singular points vs topQ
! Also generates branching points vs topQ correlation for larger than minimum topQ values

program Correlation
  use Kinds
  use FileIO
  use FancyIO
  use AnalyseLinks
  use TopQAnalysis
  use AVSCoordinates
  use MathsOps
  use ArrayOps
  implicit none

  real(DP), parameter :: prefactor = (3.0_DP*8.0_DP)/(32.0_DP*pi**2)
  integer, parameter  :: output_num = 5
  real(DP) :: V
  integer :: nx0, ny0, nz0, nt0  ! Store the original dimensions prior to shifting

  real(DP), allocatable, dimension(:,:,:,:,:,:,:) :: UR, UI
  real(DP), allocatable, dimension(:,:,:,:,:,:)   :: VSP
  real(DP), allocatable, dimension(:,:,:,:,:)     :: qmu
  real(DP), allocatable, dimension(:,:,:,:)       :: TopQ, DTopQ, MTopQ, SPts
  real(DP), allocatable, dimension(:,:,:)         :: TopQ_Slice
  real(DP), allocatable, dimension(:)             :: TopQSort, absTopQSort, qmu_AbsSort
  real(DP), allocatable, dimension(:)             :: TopQSort_Slice, absTopQSort_Slice
  integer, allocatable, dimension(:,:,:,:,:,:)    :: vortexlocation, VortexID
  integer, allocatable, dimension(:,:,:,:,:)      :: BP
  integer, allocatable, dimension(:,:,:,:)        :: BPMerge, BinaryLinkMap, SPts_int
  integer, allocatable, dimension(:,:,:)          :: BP_Slice, BP_Correlated
  real(DP), dimension(nd)                         :: Cmu, Cmu_Ideal

  real(DP) :: beta, uzero
  real(DP) :: C_Ideal, C
  integer :: i, j, nconfig1, nconfig2, N, t_hat

  ! I/O variables
  character(len=256) :: TOPQDIR, CONFIGDIR, BPTOPQDIR, VTOPQDIR
  character(len=256) :: CFG, TOPQCFG, SPTOPQDIR
  character(len=12), dimension(output_num) :: header
  character(len=6), dimension(:), allocatable :: row_labels
  character(len=4) :: the_format
  real(DP), dimension(:,:), allocatable :: output_V, output_BP, output_SP
  integer, parameter :: infl1 = 102, infl2 = 103


  ! |----------------------- Get I/O variables ------------------------|

  write(*,*) "What is the text file containing the names of the vortex only configurations?"
  read(*,*) CONFIGDIR
  write(*,*) trim(CONFIGDIR)

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt
  dims = [nx, ny, nz, nt]
  nx0 = nx
  ny0 = ny
  nz0 = nz
  nt0 = nt

  write(*,*) "What dimension is playing the role of time?"
  read(*,*) t_hat
  write(*,*) t_hat

  write(*,*) "What is the file format?"
  read(*,*) the_format

  write(*,*) "What is the text file containing the names of the topQ files?"
  read(*,*) TOPQDIR
  write(*,*) trim(TOPQDIR)

  write(*,*) "What is the output directory for the vortices vs topQ correlation data?"
  read(*,*) VTOPQDIR
  write(*,*) trim(VTOPQDIR)

  write(*,*) "What is the output directory for the branching points vs topQ correlation data?"
  read(*,*) BPTOPQDIR
  write(*,*) trim(BPTOPQDIR)

  write(*,*) "What is the output directory for the singluar points vs topQ correlation data?"
  read(*,*) SPTOPQDIR
  write(*,*) trim(SPTOPQDIR)

  open(infl1, file=trim(CONFIGDIR), status="old")
  open(infl2, file=trim(TOPQDIR), status="old")
  read(infl1,*) nconfig1
  read(infl2,*) nconfig2
  if (nconfig1 .NE. nconfig2) then
    close(infl1)
    close(infl2)
    stop("Config number doesn't match, terminating program")
  end if

  write(*,*) "Number of configs=", nconfig1
  write(*,*)

  ! |------------------------ Begin Execution -------------------------|

  ! Allocate output arrays
  allocate(output_V(nconfig1, output_num),&
      output_BP(nconfig1, output_num),&
      output_SP(nconfig1, output_num))
  allocate(row_labels(nconfig1))
  allocate(TopQSort(int(V)), absTopQSort(int(V)), qmu_AbsSort(int(V)))

  V = real(nx*ny*nz*nt, DP)

  allocate(UR(NX,NY,NZ,NT,ND,NC,NC), UI(NX,NY,NZ,NT,ND,NC,NC))

  ! Loop over configurations
  do i=1,nconfig1
    read(infl1,'(a)') CFG
    read(infl2,'(a)') TOPQCFG
    write(*,'(a)') "Vortex Cfg: ", trim(CFG)
    write(*,'(a)') "TopQ Cfg:   ", trim(TOPQCFG)
    write(*,*)

    ! Reset dims for file input
    nx = nx0
    ny = ny0
    nz = nz0
    nt = nt0
    dims = [nx, ny, nz, nt]

    ! Read the topological charge
    call ReadTopQ(TopQ, trim(TOPQCFG), t_hat)

    ! Generate a sorted topological charge array
    TopQSort = reshape(TopQ, (/int(V)/))
    absTopQSort = abs(TopQSort)
    call sort(TopQSort)
    call sort(absTopQSort)

    ! |----------------------- Vortex Correlation -----------------------|

    ! Locate Vortices

    write(*,*) "Reading", trim(CFG)
    if(the_format == "cssm") then
      call ReadGaugeField_cssm(trim(CFG),UR,UI,uzero,beta)
    else if(the_format == "ildg") then
      call ReadGaugeField_ildg(trim(CFG),UR,UI,uzero,beta)
    end if
    call MakeSU3(UR,UI)

    call LocateVortices(UR, UI, vortexlocation, t_hat)
    call set_dims(t_hat)

    ! If we're on the first config, allocate the arrays
    if(i == 1)then
      allocate(DTopQ(nx,ny,nz,nt))
      allocate(VSP(nx,ny,nz,nt,nd,nd))
      allocate(qmu(nx,ny,nz,nt,nd))
      allocate(MTopQ(nx,ny,nz,nt), SPts(nx,ny,nz,nt))
      allocate(TopQ_Slice(ny, nz, nt))
      allocate(TopQSort_Slice(int(V/nx)), absTopQSort_Slice(int(V/nx)))
      allocate(VortexID(nx,ny,nz,nt,nd,nd))
      allocate(BP(nx,ny,nz,nt,nd))
      allocate(BPMerge(nx,ny,nz,nt), BinaryLinkMap(nx,ny,nz,nt), SPts_int(nx,ny,nz,nt))
      allocate(BP_Slice(ny,nz,nt), BP_Correlated(ny,nz,nt))
    end if

    ! Calculate vortex vs topQ correlation
    BinaryLinkMap = GetBinaryLinkMap(vortexlocation)
    N = sum(BinaryLinkMap)

    call genCorrelation(BinaryLinkMap, TopQ, absTopQSort, C, C_Ideal)

    output_V(i, :) = (/C, C_Ideal, real(N, DP), mean(TopQSort), mean(absTopQSort)/)

    write(*, '(a,f8.4)')"Vortex vs TopQ Correlation: ", C

    ! |------------------ Branching Point Correlation -------------------|

    ! Locate branching points
    call LocateBranchingPoints(vortexlocation, BP)
    where((BP == 3) .OR. (BP == 5))
      BP = 1
    elsewhere
      BP = 0
    end where

    ! Calculate branching point vs topQ correlation

    ! Create TopQ averages over each 3D cube
    call SliceTopQ(TopQ, qmu)

    ! For each choice of 3D coordinates, calculate the BP correlation
    do j=1,nd
      qmu_AbsSort = abs(reshape(qmu(:,:,:,:,j), (/int(V)/)))
      call sort(qmu_AbsSort)
      call genCorrelation(BP(:,:,:,:,j), qmu(:,:,:,:,j), qmu_AbsSort, Cmu(j), Cmu_Ideal(j))
    end do

    ! Average over each dimension playing the role of time
    C = mean(Cmu)
    C_Ideal = mean(Cmu_Ideal)
    N = sum(BP)

    output_BP(i, :) = (/C, C_Ideal, real(N, DP), mean(TopQSort), mean(absTopQSort)/)

    write(*, '(a,f8.4)')"BP vs TopQ Correlation:     ", C


    ! |------------------- Singular Point Correlation -------------------|

    ! Locate singular points
    where(vortexlocation == 2)
      VSP = -sqrt(3.0_DP)/8.0_DP
    elsewhere
      VSP = sqrt(3.0_DP)/8.0_DP * real(vortexlocation, DP)
    endwhere
    ! Note that there is no negative sign (as would be expected from the epsilon tensor)
    ! on the middle term due to the spacial vortex sign we've added in
    SPts = singularcalc(VSP,2,3,1,4) + singularcalc(VSP,2,4,1,3) + singularcalc(VSP,3,4,1,2)
    SPts = abs(prefactor*SPts)

    where(SPts > tol)
      SPts_int = 1
    elsewhere
      SPts_int = 0
    endwhere
    N = sum(SPts_int)

    call genCorrelation(SPts_int, TopQ, absTopQSort, C, C_Ideal)

    output_SP(i, :) = (/C, C_Ideal, real(N, DP), mean(TopQSort), mean(absTopQSort)/)

    write(*, '(a,f8.4)')"SP vs TopQ Correlation:     ", C
    write(*,*)
    write(*,*)

  end do

  header = (/ "C", "C_Ideal", "Sum(L)", "Mean q", "Mean |q|"/)
  write(row_labels, '(i6)') (/(i, i=1,nconfig1)/)

  call Tabulate_DP(output_V, trim(VTOPQDIR), column_labels=header, row_labels=row_labels)
  call Tabulate_DP(output_BP, trim(BPTOPQDIR), column_labels=header, row_labels=row_labels)
  call Tabulate_DP(output_SP, trim(SPTOPQDIR), column_labels=header, row_labels=row_labels)

  close(infl1)
  close(infl2)

contains

  ! Shifts the topological charge to the dual lattice
  ! by taking the average over the hypercube around each point on the lattice.
  !
  subroutine DualTopQ(TopQ, DTopQ)
    real(DP), dimension(nx,ny,nz,nt), intent(in) :: TopQ
    real(DP), dimension(nx,ny,nz,nt), intent(out) :: DTopQ

    DTopQ = ( TopQ &
        + RShift_4D(TopQ, (/1,0,0,0/)) + RShift_4D(TopQ, (/0,1,0,0/)) &
        + RShift_4D(TopQ, (/0,0,1,0/)) + RShift_4D(TopQ, (/0,0,0,1/)) &
        + RShift_4D(TopQ, (/1,1,0,0/)) + RShift_4D(TopQ, (/1,0,1,0/)) &
        + RShift_4D(TopQ, (/1,0,0,1/)) + RShift_4D(TopQ, (/0,1,1,0/)) &
        + RShift_4D(TopQ, (/0,1,0,1/)) + RShift_4D(TopQ, (/0,0,1,1/)) &
        + RShift_4D(TopQ, (/1,1,1,0/)) + RShift_4D(TopQ, (/1,1,0,1/)) &
        + RShift_4D(TopQ, (/1,0,1,1/)) + RShift_4D(TopQ, (/0,1,1,1/)) &
        + RShift_4D(TopQ, (/1,1,1,1/)) )/16.0d0
  end subroutine DualTopQ

  subroutine MaxTopQ(TopQ, MTopQ)
    real(DP), dimension(nx,ny,nz,nt), intent(in) :: TopQ
    real(DP), dimension(nx,ny,nz,nt), intent(out) :: MTopQ

    ! Local vars
    real(DP), dimension(nx+1,ny+1,nz+1,nt+1) :: TopQShdw
    integer :: ix, iy, iz, it

    TopQShdw(1:nx,1:ny,1:nz,1:nt) = TopQ(:,:,:,:)
    TopQShdw(nx+1,1:ny,1:nz,1:nt) = TopQ(1,:,:,:)
    TopQShdw(1:nx,ny+1,1:nz,1:nt) = TopQ(:,1,:,:)
    TopQShdw(1:nx,1:ny,nz+1,1:nt) = TopQ(:,:,1,:)
    TopQShdw(1:nx,1:ny,1:nz,nt+1) = TopQ(:,:,:,1)

    TopQShdw(nx+1,ny+1,1:nz,1:nt) = TopQ(1,1,:,:)
    TopQShdw(nx+1,1:ny,nz+1,1:nt) = TopQ(1,:,1,:)
    TopQShdw(nx+1,1:ny,1:nz,nt+1) = TopQ(1,:,:,1)
    TopQShdw(1:nx,ny+1,nz+1,1:nt) = TopQ(:,1,1,:)
    TopQShdw(1:nx,ny+1,1:nz,nt+1) = TopQ(:,1,:,1)
    TopQShdw(1:nx,1:ny,nz+1,nt+1) = TopQ(:,:,1,1)

    do it = 1,nt
      do iz = 1,nz
        do iy = 1,ny
          do ix = 1,nx
            MTopQ(ix,iy,iz,it) = max(abs(TopQShdw(ix,iy,iz,it)), &
                abs(TopQShdw(ix+1,iy,iz,it)),     abs(TopQShdw(ix,iy+1,iz,it)), &
                abs(TopQShdw(ix,iy,iz+1,it)),     abs(TopQShdw(ix,iy,iz,it+1)), &
                abs(TopQShdw(ix+1,iy+1,iz,it)),   abs(TopQShdw(ix+1,iy,iz+1,it)), &
                abs(TopQShdw(ix+1,iy,iz,it+1)),   abs(TopQShdw(ix,iy+1,iz+1,it)), &
                abs(TopQShdw(ix,iy+1,iz,it+1)),   abs(TopQShdw(ix,iy,iz+1,it+1)), &
                abs(TopQShdw(ix+1,iy+1,iz+1,it)), abs(TopQShdw(ix+1,iy+1,iz,it+1)), &
                abs(TopQShdw(ix+1,iy,iz+1,it+1)), abs(TopQShdw(ix,iy+1,iz+1,it+1)), &
                abs(TopQShdw(ix+1,iy+1,iz+1,it+1)))
          end do
        end do
      end do
    end do

  end subroutine MaxTopQ

  ! Shifts topological charge to the dual 3D lattice
  ! Final qmu index indicates which dimension is playing the role of time
  subroutine SliceTopQ(TopQ, qmu)
    real(DP), dimension(nx,ny,nz,nt), intent(in) :: TopQ
    real(DP), dimension(nx,ny,nz,nt,nd), intent(out) :: qmu

    ! Local vars
    integer, dimension(7,nd) :: shifts
    integer :: i,j

    ! Shifts define how to obtain each site in a cube
    ! There are 7 shifts as the 8th is just the point in question
    shifts(1,:) =  (/1,0,0,0/)
    shifts(2,:) =  (/0,1,0,0/)
    shifts(3,:) =  (/0,0,1,0/)
    shifts(4,:) =  (/1,1,0,0/)
    shifts(5,:) =  (/1,0,1,0/)
    shifts(6,:) =  (/0,1,1,0/)
    shifts(7,:) =  (/1,1,1,0/)

    do i=1,nd
      qmu(:,:,:,:,i) = TopQ(:,:,:,:)
      shifts = cshift(shifts, shift = -1, dim=2)
      do j=1,7
        qmu(:,:,:,:,i) = qmu(:,:,:,:,i) + RShift_4D(TopQ, shifts(j,:))
      end do
    end do
    qmu = qmu/8.0d0

  end subroutine SliceTopQ

  ! Recursively shift over multiple dimensions
  function RShift_4D(M_in, shift) result(M_out)
    integer, parameter :: dims = 4
    real(DP), dimension(:,:,:,:), intent(in)  :: M_in
    real(DP), dimension(:,:,:,:), allocatable :: M_out
    integer, dimension(dims), intent(in) :: shift

    ! Local vars
    integer :: i

    allocate(M_out, MOLD=M_in)

    M_out = M_in

    do i = 1,dims
      if(shift(i) .NE. 0) then
        M_out = cshift(M_out, shift = shift(i), dim = i)
      end if
    end do

  end function RShift_4D

  integer(2) function comp(a1, a2)
    real(DP) :: a1,a2

    if(a1 < a2) then
      comp = -1
    else if(a1 > a2) then
      comp = 1
    else if(a1 == a2) then
      comp = 0
    end if

  end function comp

  subroutine genCorrelation(L, q, q_AbsSorted, C, C_Ideal)
    integer, dimension(nx,ny,nz,nt), intent(in)  :: L
    real(DP), dimension(nx,ny,nz,nt), intent(in) :: q
    real(DP), dimension(:), intent(in)      :: q_AbsSorted
    real(DP), intent(out) :: C
    real(DP), intent(out) :: C_Ideal

    ! Local vars
    integer :: N

    ! Calculate the correlation
    C = V * sum(abs(q) * L)/(sum(abs(q)) * sum(L)) - 1.0_DP

    ! Calculate the ideal correlation
    N = sum(L)
    C_Ideal = mean(q_AbsSorted(int(V)-N+1:int(V))) * V/sum(absTopQSort) - 1.0_DP

  end subroutine genCorrelation

end program Correlation
