! Program to calculate a variety of vortex and BP statistics
! over all slice directions, as well as the total lattice
! Calculated quantities are:
! # of BP per slice, BP volume density (lattice), BP volume density (fm^-3)
! # of vortices per slice, vortex area density (lattice), vortex area density (fm^-2)
! Naive BP branch probability (lattice), Naive BP branch probability (fm^-1)
program VortexStatistics
  use Kinds
  use FileIO
  use AnalyseLinks
  use AVSCoordinates
  use FancyIO
  use MathsOps
  use ArrayOps
  implicit none

  ! Lattice variables
  integer, dimension(nd - 1) :: s_dims ! Slice dimensions
  real(DP) :: V
  real(DP) :: a
  real(DP) :: s_V ! Slice volume

  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI
  integer, dimension(:,:,:,:,:,:), allocatable    :: Vloc
  integer, dimension(:,:,:,:,:), allocatable      :: BP, Ncube

  ! Variables to store final results.
  ! First column is values, second is errors
  ! Rows 1:4 are the different slice dimension values
  ! Fifth row is the spatial average
  ! Sixth row is the configuration average
  real(DP), dimension(nd + 2, 2) :: N_BP, N_vor
  real(DP), dimension(nd + 2, 2) :: rho_BP_lat, rho_BP_phys
  real(DP), dimension(nd + 2, 2) :: rho_vor_lat, rho_vor_phys
  real(DP), dimension(nd + 2, 2) :: prob_BP_lat, prob_BP_phys

  ! N_vor, N_BP and branching probability samples to be averaged over
  ! Dimensions are (maxval(dims), nd, ncfg)
  real(DP), dimension(:, :, :), allocatable :: N_BP_samples, N_vor_samples
  real(DP), dimension(:, :, :), allocatable :: prob_BP_samples

  integer, dimension(nd) :: x1, x2, x3
  real(DP) :: beta, uzero, N
  integer  :: ncfg, max_dim, N_pierced

  ! General variables
  integer :: icfg, j, id

  ! I/O variables
  character(len=256) :: configDir, CFG
  character(len=4)   :: the_format
  integer, parameter :: infl = 102, outfl = 103
  integer, parameter :: xfl = 104 , yfl = 105, zfl = 106, tfl = 107
  ! 16 rows in output table, nd+2 columns
  real(DP), dimension(16,nd + 2) :: output_data
  character(len=:), dimension(:), allocatable :: headers
  character(len=:), dimension(:), allocatable :: rows


  write(*,*) "What is the text file containing the names of the vortex only configurations?"
  read(*,*) configDir
  write(*,*) trim(configDir)//NEW_LINE('A')

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt

  write(*,*) "What is the file format?"
  read(*,*) the_format
  write(*,*) the_format

  write(*,*) "What is the lattice spacing in fm?"
  read(*,*) a
  write(*,*) "a = ", a

  open(infl, file=trim(configDir), status="old")
  read(infl,*) ncfg

  ! Set lattice variables
  dims = (/nx,ny,nz,nt/)
  write(*,*) 'Lattice dimensions are: ', dims
  write(*,*) 'There are: ', ncfg, ' configurations'
  V = real(nx*ny*nz*nt, DP)
  max_dim = maxval(dims)
  N = real(ncfg, DP)

  ! Allocate arrays
  allocate(UR(nx,ny,nz,nt,nd,nc,nc), UI(nx,ny,nz,nt,nd,nc,nc))
  allocate(Vloc(nx,ny,nz,nt,nd,nd))
  allocate(BP(nx,ny,nz,nt,nd), Ncube(nx,ny,nz,nt,nd))
  allocate(N_BP_samples(max_dim + 1, nd + 2, ncfg), &
      N_vor_samples(max_dim + 1, nd + 2, ncfg), &
      prob_BP_samples(max_dim + 1, nd + 2, ncfg))

  N_BP_samples = 0.0d0
  N_vor_samples = 0.0d0
  prob_BP_samples = 0.0d0

  ! Initialise indexing arrays
  x1 = (/2,1,1,1/)
  x2 = (/3,3,2,2/)
  x3 = (/4,4,4,3/)

  do icfg = 1,ncfg
    ! Open file
    read(infl,'(a)') CFG
    write(*,*) "Reading ", trim(CFG)
    write(*,*)
    ! Get vortex and branching point locations
    if(the_format == "cssm") then
      call ReadGaugeField_cssm(trim(CFG),UR,UI,uzero,beta)
    else if(the_format == "ildg") then
      call ReadGaugeField_ildg(trim(CFG),UR,UI,uzero,beta)
    end if
    call MakeSU3(UR,UI)
    call LocateVortices(UR,UI,Vloc)
    call LocateBranchingPoints(Vloc, Ncube)

    ! Identify non-trivial plaquettes
    where(Vloc .NE. 0) Vloc = 1

    ! Identify valid branching points
    where((Ncube == 3) .OR. (Ncube == 5) .OR. (Ncube == 6))
      BP = 1
    elsewhere
      BP = 0
    end where

    ! Calculate N_BP and N_vor for each dimension playing the role of time
    do id = 1, nd
      select case(id)
      case(1)
        do j = 1, dims(id)
          N_BP_samples(j, id, icfg) = real(sum(BP(j,:,:,:,id)), DP)
          N_vor_samples(j, id, icfg) = (sum(Vloc(j,:,:,:,x1(id),x2(id))) &
              + sum(Vloc(j,:,:,:,x1(id),x3(id))) &
              + sum(Vloc(j,:,:,:,x2(id),x3(id))))
          N_pierced = count(Ncube(j,:,:,:,id) .NE. 0)
          prob_BP_samples(j, id, icfg) = real(sum(BP(j,:,:,:,id)), DP) / real(N_pierced, DP)
        end do
      case(2)
        do j = 1, dims(id)
          N_BP_samples(j, id, icfg) = real(sum(BP(:,j,:,:,id)), DP)
          N_vor_samples(j, id, icfg) = (sum(Vloc(:,j,:,:,x1(id),x2(id))) &
              + sum(Vloc(:,j,:,:,x1(id),x3(id))) &
              + sum(Vloc(:,j,:,:,x2(id),x3(id))))
          N_pierced = count(Ncube(:,j,:,:,id) .NE. 0)
          prob_BP_samples(j, id, icfg) = real(sum(BP(:,j,:,:,id)), DP) / real(N_pierced, DP)
        end do
      case(3)
        do j = 1, dims(id)
          N_BP_samples(j, id, icfg) = real(sum(BP(:,:,j,:,id)), DP)
          N_vor_samples(j, id, icfg) = (sum(Vloc(:,:,j,:,x1(id),x2(id))) &
              + sum(Vloc(:,:,j,:,x1(id),x3(id))) &
              + sum(Vloc(:,:,j,:,x2(id),x3(id))))
          N_pierced = count(Ncube(:,:,j,:,id) .NE. 0)
          prob_BP_samples(j, id, icfg) = real(sum(BP(:,:,j,:,id)), DP) / real(N_pierced, DP)
        end do
      case(4)
        do j = 1, dims(id)
          N_BP_samples(j, id, icfg) = real(sum(BP(:,:,:,j,id)), DP)
          N_vor_samples(j, id, icfg) = (sum(Vloc(:,:,:,j,x1(id),x2(id))) &
              + sum(Vloc(:,:,:,j,x1(id),x3(id))) &
              + sum(Vloc(:,:,:,j,x2(id),x3(id))))
          N_pierced = count(Ncube(:,:,:,j,id) .NE. 0)
          prob_BP_samples(j, id, icfg) = real(sum(BP(:,:,:,j,id)), DP) / real(N_pierced, DP)
        end do
      end select
    end do

  end do
  close(infl)

  do icfg = 1, ncfg
    ! Average data over slice dimensions
    do id = 1, nd
      N_BP_samples(max_dim + 1, id, icfg) = mean(N_BP_samples(1:dims(id), id, icfg))
      N_vor_samples(max_dim + 1, id, icfg) = mean(N_vor_samples(1:dims(id), id, icfg))
      prob_BP_samples(max_dim + 1, id, icfg) = mean(prob_BP_samples(1:dims(id), id, icfg))

    end do
    ! Spatial averages
    N_BP_samples(max_dim + 1, nd + 1, icfg) = mean(N_BP_samples(1:dims(1), 1:nd - 1, icfg))
    N_vor_samples(max_dim + 1, nd + 1, icfg) = mean(N_vor_samples(1:dims(1), 1:nd - 1, icfg))
    prob_BP_samples(max_dim + 1, nd + 1, icfg) = mean(prob_BP_samples(1:dims(1), 1:nd - 1, icfg))

    ! Total counts
    N_BP_samples(max_dim + 1, nd + 2, icfg) = sum(N_BP_samples(1:max_dim, 1:nd, icfg))
    ! Divide by 2 to account for double-counting
    ! Arises because a vortex in the mu-nu plane has 2 orthogonal directions
    ! and therefore appears in 2 slices
    N_vor_samples(max_dim + 1, nd + 2, icfg) = sum(N_vor_samples(1:max_dim, 1:nd, icfg)) / 2.0d0
    ! prob_BP_samples(max_dim + 1, nd + 2, icfg) = sum(prob_BP_samples(1:max_dim, 1:nd, icfg)) &
    !     / sum(dims)
    prob_BP_samples(max_dim + 1, nd + 2, icfg) = mean(prob_BP_samples(max_dim + 1, 1:nd, icfg))
  end do

  ! Average over ensembles

  ! Slice averages
  do id = 1, nd + 1
    if(id <= nd) then
      s_dims = slice_dims(id)
    else
      s_dims = dims(2:nd)
    end if
    ! Define slice volume
    s_V = product(s_dims)

    ! Branching points
    N_BP(id, 1) = mean(N_BP_samples(max_dim + 1, id, :))
    N_BP(id, 2) = std(N_BP_samples(max_dim + 1, id, :), ddof=1) / sqrt(N)

    rho_BP_lat(id, :) = N_BP(id, :) / s_V
    rho_BP_phys(id, :) = rho_BP_lat(id, :) / (a ** 3)

    ! Vortices
    N_vor(id, 1) = mean(N_vor_samples(max_dim + 1, id, :))
    N_vor(id, 2) = std(N_vor_samples(max_dim + 1, id, :), ddof=1) / sqrt(N)

    rho_vor_lat(id, :) = N_vor(id, :) / (3.0d0 * s_V) ! 3 orthogonal planes
    rho_vor_phys(id, :) = rho_vor_lat(id, :) / (a ** 2)

    ! Branching probability
    prob_BP_lat(id, 1) = mean(prob_BP_samples(max_dim + 1, id, :))
    prob_BP_lat(id, 2) = std(prob_BP_samples(max_dim + 1, id, :), ddof=1) / sqrt(N)

    prob_BP_phys = prob_BP_lat / a

  end do

  ! Total quantities
  N_BP(nd + 2, 1) = mean(N_BP_samples(max_dim + 1, nd + 2, :))
  N_BP(nd + 2, 2) = jackknife(N_BP_samples(max_dim + 1, nd + 2, :))

  N_vor(nd + 2, 1) = mean(N_vor_samples(max_dim + 1, nd + 2, :))
  N_vor(nd + 2, 2) = jackknife(N_vor_samples(max_dim + 1, nd + 2, :))

  rho_BP_lat(nd + 2, 1) = N_BP(nd + 2, 1) / (nd * V)
  rho_BP_lat(nd + 2, 2) = std(N_BP_samples(max_dim + 1, nd + 2, :) / (nd * V), ddof=1) / sqrt(N)
  rho_BP_phys(nd + 2, :) = rho_BP_lat(nd + 2, :) / (a ** 3)

  rho_vor_lat(nd + 2, 1) = N_vor(nd + 2, 1) / (nplaq * V)
  rho_vor_lat(nd + 2, 2) = std(N_vor_samples(max_dim + 1, nd + 2, :) / (nplaq * V), ddof=1) / sqrt(N)
  rho_vor_phys(nd + 2, :) = rho_vor_lat(nd + 2, :) / (a ** 2)

  prob_BP_lat(nd + 2, 1) = mean(prob_BP_samples(max_dim + 1, nd + 2, :))
  prob_BP_lat(nd + 2, 2) = std(prob_BP_samples(max_dim + 1, nd + 2, :), ddof=1) / sqrt(N)
  prob_BP_phys(nd + 2, :) = prob_BP_lat(nd + 2, :) / a

  ! Output results
  output_data(1:2, :) = transpose(N_BP)
  output_data(3:4, :) = transpose(rho_BP_lat)
  output_data(5:6, :) = transpose(rho_BP_phys)
  output_data(7:8, :) = transpose(N_vor)
  output_data(9:10, :) = transpose(rho_vor_lat)
  output_data(11:12, :) = transpose(rho_vor_phys)
  output_data(13:14, :) = transpose(prob_BP_lat)
  output_data(15:16, :) = transpose(prob_BP_phys)
  headers = [character(len=12) :: "1", &
      "2", &
      "3", &
      "4", &
      "spatial", &
      "total"]

  rows = [character(len=16) :: &
      "N_BP", &
      "N_BP_err", &
      "rho_BP_lat", &
      "rho_BP_lat_err", &
      "rho_BP_phys", &
      "rho_BP_phys_err", &
      "N_vor", &
      "N_vor_err", &
      "rho_vor_lat", &
      "rho_vor_lat_err", &
      "rho_vor_phys", &
      "rho_vor_phys_err", &
      "prob_BP_lat", &
      "prob_BP_lat_err", &
      "prob_BP_phys", &
      "prob_BP_phys_err"]

  ! Save human-readable output
  call Tabulate(file="vortex_statistics_hr.log", data=output_data, column_labels=headers, &
      &   row_labels=rows, sf=10, type="fancy")
  ! Save table without extraneous formatting
  call Tabulate(file="vortex_statistics.csv", data=output_data, column_labels=headers, &
      &   row_labels=rows, sf=10, type='csv')

  write(*,*) "Program finished"

contains

  function slice_dims(id)
    integer, dimension(nd - 1) :: slice_dims
    integer, intent(in) :: id

    integer :: i, j

    j = 1
    do i = 1, nd
      if(i .NE. id) then
        slice_dims(j) = dims(i)
        j = j + 1
      end if
    end do
  end function slice_dims

end program VortexStatistics
