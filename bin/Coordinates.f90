program Coordinates

  use Kinds
  use FileIO
  use AVSCoordinates, only: singularcalc, get_arrow_coords, &
      get_singular_points_site, get_singular_points_topq, get_branching_points, get_parallel_coords
  use AnalyseLinks
  implicit none

  real(dp), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI
  real(dp), dimension(:,:,:,:), allocatable       :: singularTopQ
  integer, dimension(:,:,:,:,:,:), allocatable    :: vortexlocation
  integer, dimension(:,:,:,:), allocatable        :: singularCount, branchingPoints
  integer  :: t_hat
  real(dp) :: beta, uzero

  ! File I/O variables
  character(len=256) :: CONFIGDIR, COORDDIR
  character(len=4) :: the_format

  write(*,*) "What is the input file for the vortex only configurations?"
  read(*,*) CONFIGDIR
  write(*,*) CONFIGDIR

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt
  dims = [nx, ny, nz, nt]

  write(*,*) "What dimension is playing the role of time?"
  read(*,*) t_hat
  write(*,*) t_hat

  write(*,*) "What is the file format?"
  read(*,*) the_format
  write(*,*) the_format

  write(*,*) "What is the output directory for the plotting coordinates?"
  read(*,*) COORDDIR
  write(*,*) trim(COORDDIR)

  allocate(UR(nx,ny,nz,nt,nd,nc,nc), UI(nx,ny,nz,nt,nd,nc,nc))
  ! allocate(vortexlocation(nx,ny,nz,nt,nd,nd))
  ! allocate(singularTopQ(nx,ny,nz,nt))
  ! allocate(singularCount(nx,ny,nz,nt), branchingPoints(nx,ny,nz,nt))

  write(*,*) "Reading", trim(CONFIGDIR)
  if(the_format == "cssm") then
    call ReadGaugeField_cssm(trim(CONFIGDIR),UR,UI,uzero,beta)
  else if(the_format == "ildg") then
    call ReadGaugeField_ildg(trim(CONFIGDIR),UR,UI,uzero,beta)
  end if
  call MakeSU3(UR,UI)

  ! Get vortex locations
  call LocateVortices(UR, UI, vortexlocation, t_hat)
  call set_dims(t_hat)

  allocate(singularTopQ(nx,ny,nz,nt))
  allocate(singularCount(nx,ny,nz,nt), branchingPoints(nx,ny,nz,nt))

  write(*,*) "Generating jet coordinates"
  call get_arrow_coords(vortexlocation, trim(COORDDIR))

  write(*,*) "Generating time-oriented coordinates"
  write(*,*) "here: ", len(trim(COORDDIR))
  call get_parallel_coords(vortexlocation, trim(COORDDIR))

  write(*,*) "Locating singular points"
  call get_singular_points_topq(vortexlocation,  singularTopQ, trim(COORDDIR))
  call get_singular_points_site(vortexlocation,  singularCount, trim(COORDDIR))

  write(*,*) "Locating branching points"
  call get_branching_points(vortexlocation, branchingPoints, trim(COORDDIR))

  write(*,*) "Finished generating plotting coordinates for ", trim(CONFIGDIR)
  write(*,*)

end program Coordinates
