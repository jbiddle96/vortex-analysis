program LoopAnalysis

  use Kinds
  use FileIO
  use AVSCoordinates
  use AnalyseLinks
  use Types, only: slice_dims, Arrow, touches
  use GraphFT, only: Graph
  use MathsOps
  use ArrayOps
  use GraphAnalysis, only: build_graph, output_hist, make_hist
  implicit none

  real(dp), dimension(:,:,:,:,:,:,:), allocatable :: UR, UI
  integer, dimension(:,:,:,:,:,:), allocatable :: vortex_loc
  type(Arrow), dimension(:), allocatable, target :: arrows
  integer, dimension(:, :), allocatable :: loops
  integer, dimension(:), allocatable :: vortices_per_slice
  real(DP), dimension(:), allocatable :: cluster_extents
  integer :: ix
  integer :: N_vortices, nloops, iloop, max_vortices, max_slices, t_hat
  real(DP) :: uzero, beta

  ! BP analysis variables
  type(Graph) :: bp_graph
  integer, dimension(:,:,:,:,:), allocatable :: BP
  integer, allocatable, dimension(:) :: dists, hist

  ! File I/O variables
  character(len=256) :: config_dir, out_file, out_graph, out_hist, out_log, out_extent
  character(len=4) :: the_format
  logical :: first_graph, first_hist
  integer, parameter :: outfl = 200, graphfl = 201, histfl = 202, logfl = 203, extentfl = 204

  write(*,*) "What is the input file for the vortex only configurations?"
  read(*,*) config_dir
  write(*,*) config_dir

  write(*,*) "What is the lattice size?"
  read(*,*) nx, ny, nz, nt
  write(*,*) nx, ny, nz, nt
  dims = [nx, ny, nz, nt]

  write(*,*) "Which direction is playing the role of time?"
  read(*,*) t_hat
  write(*,*) t_hat

  write(*,*) "What is the file format?"
  read(*,*) the_format
  write(*,*) the_format

  write(*,*) "What is the output file for the loop coordinates?"
  read(*,*) out_file
  write(*,*) trim(out_file)

  write(*,*) "What is the output file for the graph data?"
  read(*,*) out_graph
  write(*,*) trim(out_graph)

  write(*,*) "What is the output file for the histogram data?"
  read(*,*) out_hist
  write(*,*) trim(out_hist)

  write(*,*) "What is the output file for the extent data?"
  read(*,*) out_extent
  write(*,*) trim(out_extent)

  write(*,*) "What is the maximum number of slices to consider?"
  write(*,*) "Enter 0 to consider all slices."
  read(*,*) max_slices
  if(max_slices == 0) max_slices = dims(t_hat)
  max_slices = min(dims(t_hat), max_slices)
  write(*,*) max_slices

  allocate(UR(nx,ny,nz,nt,nd,nc,nc), UI(nx,ny,nz,nt,nd,nc,nc))
  ! allocate(vortex_loc(nx,ny,nz,nt,nd,nd))
  allocate(vortices_per_slice(max_slices))

  write(*,*) "Reading", trim(config_dir)
  if(the_format == "cssm") then
    call ReadGaugeField_cssm(trim(config_dir),UR,UI,uzero,beta)
  else if(the_format == "ildg") then
    call ReadGaugeField_ildg(trim(config_dir),UR,UI,uzero,beta)
  end if
  call MakeSU3(UR,UI)

  write(*,*) "Locating vortices"
  ! Get vortex and BP locations
  call LocateVortices(UR, UI, vortex_loc, t_hat)
  call set_dims(t_hat)

  slice_dims = [ny, nz, nt]
  allocate(BP(nx,ny,nz,nt,nd))
  call LocateBranchingPoints(vortex_loc, BP)
  where(BP >= 3)
    BP = 1
  elsewhere
    BP = 0
  end where

  out_log = trim(out_file)//'.log'
  open(outfl, file=out_file, action='write', status='replace')

  open(logfl, file=out_log, action='write', status='replace')

  open(graphfl, file=out_graph, action='write', status='replace')
  write(graphfl, '(5(a8,x))') 'start_id', 'end_id', 'distance', 'slice', 'loop'

  open(histfl, file=out_hist, action='write', status='replace')

  open(extentfl, file=out_extent, action='write', status='replace')
  write(extentfl, '(3(a10,x),a20)') 'loop', 'slice', 'n_vortices', 'extent'

  do ix = 1, max_slices
    ! Count the vortices per slice
    vortices_per_slice(ix) = count(vortex_loc(ix,:,:,:,2:,2:) .NE. 0)
  end do

  max_vortices = maxval(vortices_per_slice(1:max_slices))
  write(*,*) "Max vortices = ", max_vortices

  do ix = 1, max_slices
    first_graph = .true.
    first_hist = .true.
    N_vortices = vortices_per_slice(ix)

    allocate(arrows(N_vortices))
    allocate(loops(N_vortices, N_vortices / 4))
    arrows%assigned = .false.

    write(*,*) 'x = ', ix

    call get_arrows(vortex_loc(ix, :, :, :, 2:, 2:), arrows)
    write(*,*) "Constructed arrows"
    call find_loops(N_vortices)
    write(*,*) "Found loops for ", N_vortices, " vortices"
    nloops = count((loops(1, :) > -1))
    allocate(cluster_extents(nloops))

    write(logfl, '(a,i0)') 'x = ', ix
    call write_loops(outfl, loops, ix)

    ! Calculate loop extents
    do iloop = 1, nloops
      call get_cluster_extent(arrows, loops(:, iloop), cluster_extents(iloop))
    end do

    call write_extents(extentfl, cluster_extents, loops, ix)

    ! Perform branching point analysis
    ! First assign branching points to each vortex they touch
    call assign_BP(BP(ix, :, :, :, 1), N_vortices)
    write(*,*) 'Assigned branching points to vortices'

    ! Iterate over identified loops
    do iloop = 1, nloops
      write(*,*)
      write(*,'(a,i0)') 'Analysing loop: ', iloop
      write(logfl, '(i0)') nloops

      ! Build the branching point graph
      call build_graph(bp_graph, arrows, loops(:, iloop))

      ! Write output if there are nodes in the graph
      if(bp_graph%n_nodes() > 0) then
        ! call calculate_distances(dists)
        call bp_graph%output_graph(graphfl, x=ix, key=iloop)
        write(*,*) 'Wrote graph'

        call bp_graph%bp_distances(dists)
        ! Dists may not be allocated if the graph is
        ! comprised entirely of 4-way points
        if(allocated(dists)) then
          call make_hist(dists, hist)
          call output_hist(hist, histfl, x=ix, key=iloop)
          write(*,*) 'Wrote histogram'
        else
          write(*,*) 'No branching points in loop, no distances were calculated.'
        end if

      else
        write(*,*) 'No branching or touching points in loop, no graph was created.'
      end if

    end do

    deallocate(arrows)
    deallocate(loops)
    deallocate(cluster_extents)
    write(*,*) '-------------'
    write(*,*)

  end do
  close(outfl)
  close(logfl)
  close(graphfl)
  close(histfl)
  close(extentfl)

contains

  subroutine get_arrows(vortex_slice, arrows)
    ! Construct an arrow array of all vortices in the given slice
    integer,dimension(:, :, :, :, :), intent(in):: vortex_slice
    type(Arrow), dimension(:) :: arrows

    integer :: counter, perpdir, val
    integer :: iy, iz, it, id, ii
    real, dimension(nd-1) :: vortex_base
    integer, dimension(nd-1) :: direction

    vortex_base=0.0
    direction=0
    counter=1

    ! Note we cannot plot any vortices that lie in an x-d plane,
    ! so we omit the x direction from nd
    do it=1,slice_dims(3)
      do iz=1,slice_dims(2)
        do iy=1,slice_dims(1)
          ! Perpdir stores the axis perpendicular to the current plane
          ! Iterates down from 3 to 1
          perpdir=3
          do id=1,nd-2
            do ii=id+1,nd - 1
              ! Set the current coordinates
              vortex_base(1) = iy
              vortex_base(2) = iz
              vortex_base(3) = it
              ! Set the plane we're looking at
              vortex_base(id) = vortex_base(id) + 0.5
              vortex_base(ii) = vortex_base(ii) + 0.5
              val = vortex_slice(iy,iz,it,id,ii)
              if(val==1) then
                ! Write arrow base coordinates
                vortex_base(perpdir) = vortex_base(perpdir)-0.5
                direction(perpdir)=1

                vortex_base = vortex_base - 1
                call arrows(counter)%initialise(vortex_base, direction, val)
                counter = counter+1

              else if(val==2) then
                ! Write arrow base coordinates
                vortex_base(perpdir) = vortex_base(perpdir)+0.5
                direction(perpdir)=-1

                vortex_base = vortex_base - 1
                call arrows(counter)%initialise(vortex_base, direction, val)
                counter = counter+1
              end if
              vortex_base=0.0
              direction=0
              perpdir = perpdir-1
            end do
          end do
        end do
      end do
    end do

  end subroutine get_arrows

  subroutine find_loops(N)
    ! Identify the independent loops present in a vortex configuration
    integer :: N ! Number of vortices

    ! Local vars
    logical :: searching
    integer, dimension(N, N / 2) :: current_loop ! Stores the current loop segments
    integer, dimension(N) :: current_segment
    integer, dimension(10) :: next_arrows ! Maximum number of touching arrows is 5
    logical, dimension(:), allocatable :: complete_segments
    integer :: n_segments, new_segments, final_loc, last_arrow
    integer :: i, j, iloop, lb, ub
    integer, dimension(2) :: idx

    searching = .true.
    loops = -1
    current_loop = -1
    current_loop(1, 1) = 1
    arrows(1)%assigned = .true.
    iloop = 1
    complete_segments = [.false.]

    do while(searching)
      n_segments = size(complete_segments)
      do i = 1, n_segments
        if(complete_segments(i)) cycle ! Skip completed segments
        next_arrows(:) = -1
        ! Find last arrow in segment
        current_segment = current_loop(:, i)
        final_loc = findloc((current_segment > 0), .true., back=.true., dim=1)
        last_arrow = current_loop(final_loc, i)
        ! Get the next arrows touching it
        next_arrows(:) = get_next_arrows(arrows(last_arrow), N)
        if(next_arrows(1) > 0) then
          ! Add the first found arrow to the current segment
          current_loop(final_loc + 1, i) = next_arrows(1)
          new_segments = size(complete_segments)
          do j = 2, 10  ! Maximum of 10 touching vortices
            ! Add additional found arrows to a new segment
            if(next_arrows(j) > 0) then
              new_segments = new_segments + 1
              current_loop(1, new_segments) = next_arrows(j)
              complete_segments = [complete_segments, .false.]
            end if
          end do
        else
          complete_segments(i) = .true.
        end if
      end do

      ! If the loop is complete, save its values into loops
      if(all(complete_segments)) then
        n_segments = size(complete_segments)
        lb = 1
        ! Merge segments into single loop
        do i = 1, n_segments
          current_segment = current_loop(:, i)
          ub = findloc((current_segment > 0), .true., back=.true., dim=1)
          loops(lb:lb + ub - 1, iloop) = current_loop(1:ub, i)
          lb = lb + ub
        end do
        write(*,*) "Completed loop ", iloop, "len = ", count(loops(:, iloop) > -1)
        iloop = iloop + 1

        ! Check if the all arrows have been accounted for
        ! This is done by checking that the sum of loops < sum 1 to N
        if(sum(loops, mask=(loops > 0)) .NE. int(N * (N + 1) / 2.0_DP)) then
          ! If not, reset the current loop and start it with an unaccounted for arrow
          current_loop = -1
          do i = 1, N
            if(.not. arrows(i)%assigned) then
              current_loop(1, 1) = i
              arrows(i)%assigned = .true.
              exit
            end if
          end do

          ! Check for all arrows assigned but routine not exiting
          if(i == (N + 1)) then
            write(*,*) 'Something is wrong'
            do i = 1, N
              idx = findloc(loops, i)
              if(all(idx == 0)) then
                write(*,*) 'Arrow ', i, ' is missing'
                write(*,*) arrows(i)
                write(*,*) arrows(i)%assigned
              end if
            end do
            exit
          end if

          deallocate(complete_segments)
          complete_segments = [.false.]
        else
          ! Else all loops are assigned and we're done
          searching = .false.
        end if
      end if
    end do

  end subroutine find_loops


  function get_next_arrows(this_arrow, N) result(next_arrows)
    type(Arrow) :: this_arrow
    integer :: N
    integer, dimension(10) :: next_arrows

    integer :: i, j, counter

    counter = 1
    next_arrows(:) = 0

    do i = 1, N
      if(.not. arrows(i)%assigned)then
        if(touches(this_arrow, arrows(i))) then

          if(counter > 5) then
            write(*,*) i
            write(*,*) this_arrow
            do j = 1, N
              if(touches(this_arrow, arrows(j))) then
                write(*,*) arrows(j)
              end if
            end do
          end if
          next_arrows(counter) = i
          arrows(i)%assigned = .true.
          counter = counter + 1
        end if
      end if
    end do

  end function get_next_arrows


  subroutine write_loops(file_unit, loops, ix, alt_colour)
    integer :: file_unit
    integer, dimension(:, :) :: loops
    integer :: ix
    logical, optional :: alt_colour

    ! Local vars
    integer :: nloops, nvortices, iloop, i, idx, total_lines, colour
    logical :: in_loop
    character(len=60) :: line
    logical :: alt_colour_

    if(present(alt_colour)) then
      alt_colour_ = alt_colour
    else
      alt_colour_ = .false.
    end if

    nloops = count(loops(1, :) > -1)

    total_lines = 0
    write(file_unit, '(2(a,i))') "x = ", ix, " Number of entries = ", max_vortices
    do iloop = 1, nloops
      in_loop = .true.
      nvortices = count(loops(:, iloop) > -1)
      do i = 1, nvortices
        idx = loops(i, iloop)
        if(alt_colour_) then
          if(iloop == 1) then
            colour = arrows(idx)%val
          else
            colour = 100 + 5 * (iloop - 1) + arrows(idx)%val
          end if

          write(line, '(8(f5.1,x))') arrows(idx)%base, real(arrows(idx)%direction), &
              real(arrows(idx)%val), real(colour)
        else
          write(line, '(8(f5.1,x))') arrows(idx)%base, real(arrows(idx)%direction), &
              real(arrows(idx)%val), real(iloop - 1)
        end if

        write(file_unit, *) trim(line)
        total_lines = total_lines + 1

      end do
    end do

    ! Pad each slice out to max_vortices lines
    do i = 1, (max_vortices - total_lines)
      write(file_unit, *) trim(line)
    end do

  end subroutine write_loops


  subroutine assign_BP(BP, N)
    ! Assign to each arrow whether it is associated with a branching point
    integer, dimension(:, :, :), intent(in) :: BP
    integer :: N

    ! Local vars
    real(SP), dimension(:, :), allocatable :: BP_loc
    integer :: nBP, counter, i, j, it, iz, iy
    integer, dimension(:), allocatable :: assigned_BP

    nBP = count(BP > 0)
    write(*,*) "# Branching points = ", nBP
    allocate(BP_loc(3, nBP))
    allocate(assigned_BP(nBP))

    ! Get the coordinates of each branching point
    counter = 1
    do it = 1, slice_dims(3)
      do iz = 1, slice_dims(2)
        do iy = 1, slice_dims(1)
          if(BP(iy, iz, it) == 1) then
            BP_loc(:, counter) = [real(iy - 1) + 0.5, real(iz - 1) + 0.5, real(it - 1) + 0.5]
            counter = counter + 1
          end if
        end do
      end do
    end do

    ! Assign the branching points to vortices
    assigned_BP(:) = 0
    do j = 1, nBP
      do i = 1, N
        if(all(abs(arrows(i)%tip - BP_loc(:, j)) < tol)) then
          call arrows(i)%set_BP(j, 't')
          assigned_BP(j) = assigned_BP(j) + 1
        else if(all(abs(arrows(i)%base - BP_loc(:, j)) < tol)) then
          call arrows(i)%set_BP(j, 'b')
          assigned_BP(j) = assigned_BP(j) + 1
        end if
      end do
    end do

    ! Check if any BP are unphysical
    if(any(assigned_BP < 3)) then
      write(*,*) "BP not assigned, aborting"
      stop
    end if

  end subroutine assign_BP


  subroutine get_cluster_extent(arrows, loop, extent)
    ! Calculate the cluster extent for a given vortex loop
    ! Cluster extent is defined as the maximum pairwise distance
    type(Arrow), dimension(:), target :: arrows
    integer, dimension(:) :: loop
    real(DP) :: extent

    ! Local vars
    real, dimension(3) :: diff
    real(DP) :: d, max_extent
    type(Arrow) :: arrow1, arrow2
    integer :: n_arrows_in_loop, i, j

    n_arrows_in_loop = count(loop > -1)
    max_extent = norm2(slice_dims / 2.0d0)

    extent = 0
    do i = 1, n_arrows_in_loop - 1
      arrow1 = arrows(loop(i))
      do j = i + 1, n_arrows_in_loop
        arrow2 = arrows(loop(j))

        diff = abs(arrow1%base - arrow2%base)
        where(diff > slice_dims / 2)
          diff = diff - slice_dims
        end where
        d = norm2(real(diff, DP))

        ! Keep track of the max value
        if(d > extent) extent = d
        ! If the maximum extent is reached, exit
        if(extent >= max_extent) exit
      end do
    end do

    ! Normalise the extents
    extent = extent / max_extent

  end subroutine get_cluster_extent

  subroutine write_extents(file_unit, extents, loops, ix)
    integer :: file_unit
    real(DP), dimension(:) :: extents
    integer, dimension(:, :) :: loops
    integer :: ix

    ! Local vars
    integer :: iloop, nloops, n_vortices

    nloops = size(extents)
    do iloop = 1, nloops
      n_vortices = count(loops(:, iloop) > -1)
      write(file_unit,'(3(i10,x),f20.12)') iloop, ix, n_vortices, extents(iloop)
    end do

  end subroutine write_extents

end program LoopAnalysis
