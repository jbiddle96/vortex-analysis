# Makefile designed to enter 2 directories and execute make in each of them
# ./src/ contains the module files
# ./bin/ contains the program files

BIN = ./bin/
SRC = ./src/
SUBDIRS = ${BIN} ${SRC}

all: depends ${SUBDIRS}

# Loop through the directories and execute 'make' in each of them
# ${MAKE} is a predefined variable that corresponds to typing make in the terminal
# -C is for executing commands
# $@ references the current directory being made (google makefile wildcards for more info)
${SUBDIRS}:
	${MAKE} -C $@

# Generate dependency files
depends:
	mkdeps.py ${BIN} ${SRC} -v

# Loop through directories and 'make clean' in each of them
clean:
	$(foreach dir, ${BIN} ${SRC}, ${MAKE} -C ${dir} clean;)

# Must tell the makefile that everything in ./bin/ depends on the contents of ./src/
${BIN}: ${SRC}

# .PHONY target is used when a rule doesn't correspond to a file.
# Essentially it stops the makefile from checking if a file is up to date,
# which wouldn't make sense for a directory
.PHONY: all clean depends ${SUBDIRS}
