F90C      = ifort
F90FLAGS  = -O2 -ip -assume byterecl -convert big_endian # -shared-intel -mcmodel=medium
# DEBUGFLAGS = -traceback -check bounds -warn all

INCLUDE = -I../src/

%.o: %.f90
	$(F90C) $(F90FLAGS) $(DEBUGFLAGS) -c $(INCLUDE) $<

%.x: %.f90
	$(F90C) $(F90FLAGS) $(DEBUGFLAGS) -o $@ $(INCLUDE) $^
