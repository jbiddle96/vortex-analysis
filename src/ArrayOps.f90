module ArrayOps
  use Kinds
  implicit none

  interface shift_index
    module procedure shift_index_2d_int
    module procedure shift_index_3d_int
    module procedure shift_index_4d_int
    module procedure shift_index_5d_int
    module procedure shift_index_6d_int
    module procedure shift_index_2d_DP
    module procedure shift_index_3d_DP
    module procedure shift_index_4d_DP
    module procedure shift_index_5d_DP
    module procedure shift_index_6d_DP
  end interface shift_index

contains

  subroutine shift_index_2d_int(in_arr, out_arr, dim)
    ! Reshape an array such that the dim'th index becomes the first index
    integer, dimension(:, :) :: in_arr
    integer, dimension(:, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(2) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_2d_int


  subroutine shift_index_3d_int(in_arr, out_arr, dim)
    ! Reshape an array such that the dim'th index becomes the first index
    integer, dimension(:, :, :) :: in_arr
    integer, dimension(:, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(3) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_3d_int


  subroutine shift_index_4d_int(in_arr, out_arr, dim)
    integer, dimension(:, :, :, :) :: in_arr
    integer, dimension(:, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(4) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), out_shape(4)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_4d_int


  subroutine shift_index_5d_int(in_arr, out_arr, dim)
    integer, dimension(:, :, :, :, :) :: in_arr
    integer, dimension(:, :, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(5) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), out_shape(4), out_shape(5)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_5d_int

  subroutine shift_index_6d_int(in_arr, out_arr, dim)
    integer, dimension(:, :, :, :, :, :) :: in_arr
    integer, dimension(:, :, :, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(6) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), &
        out_shape(4), out_shape(5), out_shape(6)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_6d_int


  subroutine shift_index_2d_DP(in_arr, out_arr, dim)
    ! Reshape an array such that the dim'th index becomes the first index
    real(DP), dimension(:, :) :: in_arr
    real(DP), dimension(:, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(2) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_2d_DP


  subroutine shift_index_3d_DP(in_arr, out_arr, dim)
    ! Reshape an array such that the dim'th index becomes the first index
    real(DP), dimension(:, :, :) :: in_arr
    real(DP), dimension(:, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(3) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_3d_DP


  subroutine shift_index_4d_DP(in_arr, out_arr, dim)
    real(DP), dimension(:, :, :, :) :: in_arr
    real(DP), dimension(:, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(4) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), out_shape(4)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_4d_DP


  subroutine shift_index_5d_DP(in_arr, out_arr, dim)
    real(DP), dimension(:, :, :, :, :) :: in_arr
    real(DP), dimension(:, :, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(5) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), out_shape(4), out_shape(5)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_5d_DP

  subroutine shift_index_6d_DP(in_arr, out_arr, dim)
    real(DP), dimension(:, :, :, :, :, :) :: in_arr
    real(DP), dimension(:, :, :, :, :, :), allocatable :: out_arr
    integer :: dim

    ! Local vars
    integer :: rank, i, counter
    integer, dimension(6) :: order, in_shape, out_shape

    in_shape = shape(in_arr)
    rank = size(in_shape)

    if(dim > rank) then
      write(*,*) 'dim is larger than array rank, exiting'
      call exit(0)
    end if

    if(allocated(out_arr)) deallocate(out_arr)

    order(dim) = 1
    counter = 2
    do i = 1, rank
      if(i .NE. dim) then
        order(i) = counter
        counter = counter + 1
      end if
    end do

    out_shape(1) = in_shape(dim)
    counter = 1
    do i = 2, rank
      if(counter == dim) counter = counter + 1
      out_shape(i) = in_shape(counter)
      counter = counter + 1
    end do

    allocate(out_arr(out_shape(1), out_shape(2), out_shape(3), &
        out_shape(4), out_shape(5), out_shape(6)))

    out_arr = reshape(in_arr, out_shape, order=order)

  end subroutine shift_index_6d_DP


  subroutine get_submatrix(in_mat, out_mat, dim)
    ! Determine in_mat with the dim'th row and column removed
    integer, dimension(:, :), intent(in) :: in_mat
    integer, dimension(:, :), intent(out) :: out_mat
    integer :: dim

    if(any((shape(in_mat) - shape(out_mat)) .NE. [1, 1])) then
      write(*,*) 'Shape mismatch in get_submatrix, aborting'
      call exit(0)
    end if

    ! Top left
    out_mat(1:dim - 1, 1:dim - 1) = in_mat(1:dim - 1, 1:dim - 1)

    ! Bottom right
    out_mat(dim:, dim:) = in_mat(dim + 1:, dim + 1:)

    ! Bottom left
    out_mat(dim:, 1:dim - 1) = in_mat(dim + 1:, 1:dim - 1)

    ! Top right
    out_mat(1:dim - 1, dim:) = in_mat(1:dim - 1, dim + 1:)

  end subroutine get_submatrix

end module ArrayOps
