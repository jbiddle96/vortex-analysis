module AnalyseLinks
  use ArrayOps
  use Kinds
  implicit none

contains

  subroutine set_dims(t_hat)
    ! Set the dims to have the t_hat index first
    ! with all others following in their original order
    ! This results in dims matching the shape of the first 4 indices of the
    ! output of LocateVortices and ReadTopQ routines
    integer, intent(in) :: t_hat

    dims = [dims(t_hat), dims(:t_hat - 1), dims(t_hat + 1:)]
    nx = dims(1)
    ny = dims(2)
    nz = dims(3)
    nt = dims(4)

  end subroutine set_dims

  ! Subroutine to find the distribution of centre elements
  subroutine LinkDensity(UR,UI,linkcount)

    real(dp),dimension(:,:,:,:,:,:,:),intent(in) :: UR,UI
    integer, dimension(nc), intent(out) :: linkcount
    integer :: ix,iy,iz,it,id
    linkcount = 0
    ! Count how many of each link type is present
    do it=1,NT
       do iz=1,NZ
          do iy=1,NY
             do ix=1,NX
                do id =1,ND
                   if(abs(UI(ix,iy,iz,it,id,1,1) + sqrt(3.0d0)/2.0d0) < 0.1) then
                      linkcount(1) = linkcount(1)+1
                   else if(abs(UR(ix,iy,iz,it,id,1,1) - 1.0d0) < 0.1) then
                      linkcount(2) = linkcount(2)+1
                   else if(abs(UI(ix,iy,iz,it,id,1,1) -  sqrt(3.0d0)/2.0d0) < 0.1) then
                      linkcount(3) = linkcount(3)+1
                   end if
                end do
             end do
          end do
       end do
    end do
  end subroutine LinkDensity

  ! Subroutine to create an array of vortex locations
  ! Syntax is: V(x,y,z,t, direction 1, direction 2) = 1, 2
  ! i.e. V(1,2,1,1,x,y) = 1 is a positive vortex from site (1,2,1,1) in the x-y plane
  ! and V(1,2,1,1,x,y) = 2 is a negative vortex from site (1,2,1,1) in the x-y plane
  subroutine LocateVortices(UR, UI, V, t_hat)

    ! IO variables
    real(dp), dimension(:,:,:,:,:,:,:), intent(in) :: UR,UI
    integer, dimension(:,:,:,:,:,:), allocatable :: V
    integer, intent(in), optional :: t_hat ! Which direction is playing the role of time

    ! Local variables
    integer, dimension(nx,ny,nz,nt,nd)  :: UZ3
    integer, dimension(nx,ny,nz,nt,nd,nd)  :: W
    real(DP), dimension(nx,ny,nz,nt,nd) :: phase
    integer, dimension(5, 4) :: ss ! subscripts
    integer, dimension(6) :: sign_arr
    integer, dimension(nd) :: xp1
    integer :: pvalue
    integer :: t_hat_
    integer :: ix,iy,iz,it,is,mu,nu

    ! Initialise variables
    if(present(t_hat)) then
      t_hat_ = t_hat
    else
      t_hat_ = 1
    end if

    if(allocated(V)) deallocate(V)

    ! Define the right-hand rule signs for each dimension acting as time
    select case(t_hat_)
    case(1)
      sign_arr = [1, 1, 1, 1, -1, 1]
    case(2)
      sign_arr = [1, 1, -1, 1, 1, 1]
    case(3)
      sign_arr = [1, 1, -1, 1, 1, 1]
    case(4)
      sign_arr= [1, -1, 1, 1, 1, 1]
    end select

    W = 0

    phase = atan2(UI(:,:,:,:,:,1,1), UR(:,:,:,:,:,1,1))
    where (phase < pi/3.0d0 .AND. phase >= -pi/3.0d0) UZ3(:,:,:,:,:) = 0
    where (phase < pi .AND. phase >= pi/3.0d0) UZ3(:,:,:,:,:) = +1
    where (phase < -pi/3.0d0 .AND. phase >= -pi) UZ3(:,:,:,:,:) = -1

    ! Identify the non-trivial p-vortices
    do it=1,NT
      do iz=1,NZ
        do iy=1,NY
          do ix=1,NX
            is = 1
            do mu=1,ND-1

              call shiftx([ix, iy, iz, it], xp1, +1)

              do nu = mu + 1, nd
                ! Make the subscripts
                ss(:, :) = spread([ix, iy, iz, it, mu], 2, 4)
                ss(mu, 2) = xp1(mu)
                ss(5, 2) = nu
                ss(nu, 3) = xp1(nu)
                ss(5, 4) = nu

                ! Calculate the plaquette value
                pvalue = sign_arr(is) * (UZ3(ss(1, 1), ss(2, 1), ss(3, 1), ss(4, 1), ss(5, 1)) &
                    +UZ3(ss(1, 2), ss(2, 2), ss(3, 2), ss(4, 2), ss(5, 2)) &
                    -UZ3(ss(1, 3), ss(2, 3), ss(3, 3), ss(4, 3), ss(5, 3)) &
                    -UZ3(ss(1, 4), ss(2, 4), ss(3, 4), ss(4, 4), ss(5, 4)))

                W(ix, iy, iz, it, mu, nu) = modulo(pvalue, 3)
                is = is + 1
              end do

            end do
            call reorder_vortices(W(ix, iy, iz, it, :, :), t_hat_)

          end do
        end do
      end do
    end do

    ! Shift the t_hat index to be the first index
    call shift_index(W, V, t_hat_)
  end subroutine LocateVortices

  ! Function to determine whether lattice sites are associated with a vortex
  ! Site is assigned a 1 if it is, or a 0 if not
  function GetBinaryLinkMap(V)
    integer, dimension(:,:,:,:,:,:),intent(in):: V
    integer  :: i,j
    integer, dimension(nx,ny,nz,nt):: getbinarylinkmap

    do i=1,nd-1
       do j=i+1,nd
          getbinarylinkmap = ( V(:,:,:,:,i,j) &
               + cshift(V(:,:,:,:,i,j), shift=-1, dim=i) &
               + cshift(V(:,:,:,:,i,j), shift=-1, dim=j) &
               + cshift(cshift(V(:,:,:,:,i,j), shift=-1, dim=i), shift=-1, dim=j) )
       end do
    end do

    where(getbinarylinkmap > 0) getbinarylinkmap = 1

  end function GetBinaryLinkMap

  ! Function to get a binary correlation between topological charge and vortex locations
  function getbinarycorrelation(binarylinkmap,dyntopQ,lowerq,upperq)

    integer,dimension(:,:,:,:),intent(in) :: binarylinkmap
    real(dp),dimension(:,:,:,:),intent(in):: dyntopQ
    real(dp) :: getbinarycorrelation
    real(dp), intent(in) :: lowerq, upperq
    integer,dimension(nx,ny,nz,nt) :: binarytopq

    integer :: ix,iy,iz,it

    do ix=1,nx
       do iy=1,ny
          do iz=1,nz
             do it=1,nt
                if (dyntopQ(ix,iy,iz,it).ge.upperq .or. dyntopQ(ix,iy,iz,it).le.lowerq) then
                !call random_number(x)
                !if(x<0.5_dp) then
                   binarytopq(ix,iy,iz,it)=1
                else
                   binarytopq(ix,iy,iz,it)=0
                end if
             end do
          end do
       end do
    end do

    getbinarycorrelation =sum(binarytopq*binarylinkmap)/&
         real(min(sum(binarytopq),sum(binarylinkmap)))
  end function getbinarycorrelation

  subroutine LocParallelVortices(Vloc, parallelcheck, ndiff)
    integer, dimension(:,:,:,:,:,:), intent(in) :: Vloc
    integer, dimension(:,:,:,:,:,:), intent(out) :: parallelcheck
    integer, intent(out) :: ndiff

    integer, dimension(nx,ny,nz,nt,nd,nd) :: Vplus, Vminus
    integer :: i, j

    ! Separate vortex array into plus and minus vortices
    Vplus = 0
    Vminus = 0
    where(Vloc == 1) Vplus = 1
    where(Vloc == 2) Vminus = 1

    do i=1,nd-1
       do j=i+1,nd
          parallelcheck(:,:,:,:,i,j) = ParallelSum(Vplus, i, j) + 2*ParallelSum(Vminus, i, j)
       end do
    end do

    ndiff = count(parallelcheck(:,:,:,:,:,:) .NE. Vloc(:,:,:,:,:,:))

  contains
    function ParallelSum(V, i, j) result(PS)
      integer, dimension(:,:,:,:,:,:) :: V
      integer, dimension(nx,ny,nz,nt) :: PS
      integer, intent(in) :: i, j

      PS = 0
      where(V(:,:,:,:,i,j) .NE. 0)
      PS(:,:,:,:) = V(:,:,:,:,i,j) &
           + cshift(V(:,:,:,:,i,j), shift = 1, dim = i) &
           + cshift(V(:,:,:,:,i,j), shift = 1, dim = j) &
           + cshift(V(:,:,:,:,i,j), shift = -1, dim = i) &
           + cshift(V(:,:,:,:,i,j), shift = -1, dim = j)
      end where

    end function ParallelSum

  end subroutine LocParallelVortices

  subroutine LocateBranchingPoints(Vloc, BP)
    ! Sum the number of vortices entering each cube for each 3D slice
    integer, dimension(:,:,:,:,:,:), intent(in) :: Vloc
    integer, dimension(:,:,:,:,:), intent(out) :: BP

    ! Local vars
    integer, dimension(nx,ny,nz,nt,nd,nd) :: V
    integer, dimension(nd) :: x1, x2, x3
    integer :: i

    ! Index vectors to construct the correct vortex planes
    x1 = (/2,1,1,1/)
    x2 = (/3,3,2,2/)
    x3 = (/4,4,4,3/)

    V = Vloc
    where(V == 2) V = 1

    BP = 0
    do i = 1,nd
      ! Take into account all 4 combinations of 3 dimensions
      BP(:,:,:,:,i) = V(:,:,:,:,x1(i),x2(i)) &
            + V(:,:,:,:,x1(i),x3(i)) &
            + V(:,:,:,:,x2(i),x3(i)) &
            + cshift( V(:,:,:,:,x1(i),x2(i)), shift = +1, dim = x3(i)) &
            + cshift( V(:,:,:,:,x1(i),x3(i)), shift = +1, dim = x2(i)) &
            + cshift( V(:,:,:,:,x2(i),x3(i)), shift = +1, dim = x1(i))
    end do
  end subroutine LocateBranchingPoints

  subroutine shiftx(x, xshift, shift, mu)
    ! Periodically shift x by amount shift
    ! If mu is present, only shift the mu direction
    integer, dimension(4), intent(in) :: x
    integer, dimension(4), intent(out) :: xshift
    integer, intent(in) :: shift
    integer, intent(in), optional :: mu

    if(present(mu)) then
      xshift = x
      xshift(mu) = xshift(mu) + shift
    else
      xshift = x + shift
    end if

    where(xshift > dims)
      xshift = xshift - dims
    end where

    where(xshift <= 0)
      xshift = xshift + dims
    end where

  end subroutine shiftx

  subroutine reorder_vortices(arr, dim)
    ! Reorder a nd X nd vortex array such that t_hat is the first row and column index.
    ! This routine maintains the original order of the other columns
    ! Then move any elements from the lower diagonal into the upper diagonal and flip their vortex sign
    ! This maintains the upper-diagonal convention
    integer, dimension(nd, nd) :: arr
    integer, intent(in) :: dim

    ! Local vars
    integer, dimension(nd) :: prev_vec, temp_vec
    integer :: i, j

    ! Shift row
    prev_vec = arr(dim, :)
    do i = 1, dim
      temp_vec = arr(i, :)
      arr(i, :) = prev_vec
      prev_vec = temp_vec
    end do

    ! Shift column
    prev_vec = arr(:, dim)
    do i = 1, dim
      temp_vec = arr(:, i)
      arr(:, i) = prev_vec
      prev_vec = temp_vec
    end do

    ! Fix transpose
    do j = 1, nd - 1
      do i = j + 1, nd
        if(arr(i, j) == 2) then
          arr(j, i) = 1
          arr(i, j) = 0
        end if
        if(arr(i, j) == 1)then
          arr(j, i) = 2
          arr(i, j) = 0
        end if
      end do
    end do

  end subroutine reorder_vortices

end module AnalyseLinks
