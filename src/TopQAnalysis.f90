module TopQAnalysis
  use Kinds
  use ArrayOps
  use AnalyseLinks
  implicit none

contains

  subroutine ReadTopQ(TopQ, CIN, t_hat)
    character(len=*), intent(in)  :: CIN
    real(DP), dimension(:,:,:,:), allocatable, intent(out) :: TopQ
    integer, intent(in), optional :: t_hat

    real(DP), dimension(nx,ny,nz,nt) :: TopQ_init
    integer :: t_hat_

    if(present(t_hat)) then
      t_hat_ = t_hat
    else
      t_hat_ = 1
    end if

    open (101,file=CIN,status='old',form='unformatted')
    read(101) TopQ_init
    close(101)

    if(allocated(TopQ)) deallocate(TopQ)
    call shift_index(TopQ_init, TopQ, t_hat_)
  end subroutine ReadTopQ

  subroutine TopQDiffSP(TopQ, SP)
    real(DP), dimension(nx,ny,nz,nt),intent(in) :: TopQ, SP
    integer :: ndiff
    integer :: ix, iy, iz, it

    open(101, file="diffTopQSP.data", form="formatted",status="replace")
    do it = 1,nt
       do iz = 1,nz
          do iy = 1,ny
             do ix = 1,nx
                
                if(abs(TopQ(ix,iy,iz,it) - SP(ix,iy,iz,it))>10*epsilon(1.0_DP)) then
                   write(101,'(a,f10.6)') "TopQ = ", TopQ(ix,iy,iz,it)
                   write(101,'(a,f10.6)') "SingularTopQ = ", SP(ix,iy,iz,it)
                   write(101,'(a,f10.6)') "TopQ - SingularTopQ = ", TopQ(ix,iy,iz,it) &
                       - SP(ix,iy,iz,it)
                end if
             end do
          end do
       end do
    end do
    close(101)
    
    ndiff = count(abs(TopQ - SP)>10*epsilon(1.0_DP))
    
    write(*,*) "Topological charge and singular points differ at ", ndiff, "locations"

  end subroutine TopQDiffSP
  
  !  Subroutine to generate AVS coordinates for plotting TopQ Density
  subroutine ChargeCoords(action,dyntopQ,CIN,COUT, scale_output, t_hat)

    real(DP), dimension(:,:,:,:), allocatable, intent(inout) :: action
    real(DP), dimension(:,:,:,:), allocatable, intent(out) :: dyntopQ
    character(len=*), intent(in)  :: CIN,COUT
    logical, optional :: scale_output  ! Toggle outputting the raw or scaled topQ
    integer, optional :: t_hat  ! Dimension playing the role of time

    real(DP)  :: minAction, maxAction, avgAction, rmsAction 
    real(DP)  :: avgMaxAction, topQ 
    real(DP)  :: scaleaction
    real(DP)  :: minact, maxact 
    real(DP)  :: dynamicRange = 5.0d0 
    integer :: ix, iy, iz, it
    logical :: scale_
    integer :: t_hat_

    if(present(t_hat)) then
      t_hat_ = t_hat
    else
      t_hat_ = 1
    end if

    if(present(scale_output)) then
      scale_ = scale_output
    else
      scale_ = .false.
    end if

    ! Open input file
    call ReadTopQ(action, CIN, t_hat_)
    call set_dims(t_hat_)
    allocate(dyntopQ(nx, ny, nz, nt))

    ! Create output file name
    open(11,file=COUT//'.summary',status='replace',form='formatted')
    open(12,file=COUT,status='replace',form='formatted')

    !  Compute some general properties
    topQ      = sum(action) 
    avgAction = sum(abs(action))/real(nx*ny*nz*nt, DP)
    rmsAction = sqrt(sum(action**2)/real(nx*ny*nz*nt, DP))
    maxAction = maxval(action) 
    minAction = minval(action) 
    avgMaxAction = 0.0d0
    do ix=1,nx
      avgMaxAction = avgMaxAction + maxval(action(ix,:,:,:))          &
          &                              - minval(action(ix,:,:,:))
    end do
    avgMaxAction = avgMaxAction / nx / 2.0d0

    write(11,*) 
    write(11,'(a,f13.8,a)') 'Topological charge is ',topQ,'.' 
    write(11,'(a,f11.8,a,f11.8,a)')                                 &
        &                          'Values range from     ',minAction,     &
        &                          ' to ',maxAction,'.'
    write(11,'(a,f11.8,a)') 'The rms value is      ',rmsAction,'.' 
    write(11,'(a,f11.8,a)') 'The average value is  ',avgAction,'.' 
    write(11,'(a,f11.8,a)') 'The avg. max. / slice ',avgMaxAction,'.'

    avgMaxAction = 0.0d0

    do ix = 1,nx                                          

      maxact = maxval(action(ix,:,:,:))
      minact = minval(action(ix,:,:,:))

      write(11,'(a,i2,a,f11.8,a,f11.8)')                   &
          &        'The maximum/minimum t.c. for slice ', &
          &         ix,' is ',maxact,' and ',minact
      write(11,*)
      !
      !  Write data to file
      !
      write(12,'(a,i3,a,i)') 'x= ',ix, ' Number of Entries= ',ny*nz*nt

      ! Fix the dynamic range of the topQ
      scaleAction = min(avgAction*dynamicRange, maxAction)

      do it = 1, nt
        do iz = 1, nz
          do iy = 1, ny

            if(scale_)then
              dyntopQ(ix,iy,iz,it) =  max(                                            &
                  &          min( (action(ix,iy,iz,it) + scaleAction) *              &
                  &               255.0d0 / scaleAction / 2.0d0, 255.0d0 ),          &
                  &          0.0d0)

              write(12,'(3(f,A),f10.5)') real(iy)," ", real(iz)," ",real(it)," ", dyntopQ(ix,iy,iz,it)
            else
              write(12,'(3(f,A),f)') real(iy)," ", real(iz)," ",real(it)," ", action(ix,iy,iz,it)
            end if
          end do
        end do
      end do
    end do
    close(12) 
    close(11)

  end subroutine ChargeCoords
  
end module TopQAnalysis
