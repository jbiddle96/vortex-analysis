module GraphAnalysis
  use Types
  use Kinds
  use MathsOps, only: random_index
  use GraphFT
  implicit none

contains


  subroutine build_graph(bp_graph, arrows, loop)
    type(Graph) :: bp_graph
    type(Arrow), dimension(:), target :: arrows
    integer, dimension(:) :: loop

    ! Local vars
    type(Arrow), pointer :: current_arrow
    integer, dimension(:), allocatable :: BP_ids
    integer :: n_arrows_in_loop, i, nBP, this_id, this_dist, start_id
    logical :: find_new
    integer :: iter, start_idx, this_idx

    arrows%assigned = .false.
    n_arrows_in_loop = count(loop > -1)

    ! Find a vortex with the branching point at the base
    start_id = -1
    do i = 1, n_arrows_in_loop
      if(arrows(loop(i))%BP_id(2) > -1) then
        current_arrow => arrows(loop(i))
        ! Save branching point ID
        start_id = current_arrow%BP_id(2)
        exit
      end if
    end do

    if(start_id == -1) then
      call bp_graph%delete_graph()
      return
    end if

    ! Get the unique BP ids in the loop
    call get_unique_ids(arrows, loop, BP_ids)

    nBP = size(BP_ids)

    ! Initialise the graph
    call Graph(bp_graph, nBP)

    this_dist = 1
    iter = 0
    find_new = .false.
    do while(.true.)
      if(find_new) then
        ! If necessary, find a new start point
        do i = 1, n_arrows_in_loop
          if(arrows(loop(i))%BP_id(2) > -1 .and. (.not. arrows(loop(i))%assigned)) then
            current_arrow => arrows(loop(i))
            find_new = .false.
            ! Save branching point ID
            start_id = current_arrow%BP_id(2)
            exit
          end if
        end do
      end if

      ! If no new branching point can be found to start at, then we're done
      if(find_new) then
        do i = 1, n_arrows_in_loop
          if(.not. arrows(loop(i))%assigned) then
            write(*,*) arrows(loop(i))
            write(*,*) arrows(loop(i))%BP_id
            write(*,*)
          end if
        end do
        exit
      end if

      if(current_arrow%BP_id(1) > -1) then
        ! If we've reached a BP at the tip, add it to the graph
        current_arrow%assigned = .true.
        this_id = current_arrow%BP_id(1)
        start_idx = findloc(BP_ids, start_id, dim=1)
        this_idx = findloc(BP_ids, this_id, dim=1)
        call bp_graph%add_edge(start_idx, this_idx, this_dist)
        this_dist = 1
        find_new = .true.

      else
        ! Else continue along the current path
        do i = 1, n_arrows_in_loop
          if(all(abs(arrows(loop(i))%base - current_arrow%tip) < tol) &
              .and. (.not. arrows(loop(i))%assigned)) then
            current_arrow%assigned = .true.
            current_arrow => arrows(loop(i))
            this_dist = this_dist + 1
            exit
          end if
        end do
      end if

      iter = iter + 1
    end do

    write(*,*) "Built graph"
    write(*,'(a,i0)') "Number of vortices   = ", n_arrows_in_loop

    ! Mark the nodes as a BP or not
    ! call identify_BP()
  end subroutine build_graph


  subroutine get_unique_ids(arrows, loop, ids)
    ! Determine the unique branching point ids in a loop
    type(Arrow), dimension(:), intent(in) :: arrows
    integer, dimension(:), intent(in) :: loop
    integer, dimension(:), allocatable :: ids

    ! Local vars
    logical, dimension(:), allocatable :: larray
    integer :: i, n_arrows, max_id

    if(allocated(ids)) deallocate(ids)

    n_arrows = count(loop > -1)

    ! Figure out the largest present id
    max_id = -1
    do i = 1, n_arrows
      if(arrows(loop(i))%BP_id(1) > max_id) max_id = arrows(loop(i))%BP_id(1)
      if(arrows(loop(i))%BP_id(2) > max_id) max_id = arrows(loop(i))%BP_id(2)
    end do

    allocate(larray(max_id))
    larray = .false.

    ! Build a logical array that is true where a BP id is present
    do i = 1, n_arrows
      if(arrows(loop(i))%BP_id(1) > 0) larray(arrows(loop(i))%BP_id(1)) = .true.
      if(arrows(loop(i))%BP_id(2) > 0) larray(arrows(loop(i))%BP_id(2)) = .true.
    end do

    ! write(*,*) 'ids before = ', ids
    ! Use the logical array to assign the id array
    do i = 1, max_id
      if(larray(i)) then
        if(allocated(ids)) then
          ids = [ids, i]
        else
          ids = [i]
        end if
      end if
    end do

  end subroutine get_unique_ids


  subroutine make_hist(dists, hist)
    ! Make a histogram of the branching point separations
    integer, dimension(:), intent(in) :: dists
    integer, dimension(:), allocatable, intent(out) :: hist

    ! Local vars
    integer :: max_dist, i

    max_dist = maxval(dists)

    if(allocated(hist)) deallocate(hist)
    allocate(hist(max_dist))

    do i = 1, max_dist
      hist(i) = count(dists == i)
    end do

  end subroutine make_hist


  subroutine output_hist(hist, unit, x, key)
    integer, dimension(:), intent(in) :: hist

    integer, intent(in) :: unit
    integer, intent(in), optional :: x ! Slice number
    integer, intent(in), optional :: key ! Used to distinguish separate graphs in the same slice

    ! Local vars
    integer :: x_, key_
    integer :: i

    if(present(key)) then
      key_ = key
    else
      key_ = ''
    end if

    if(present(x)) then
      x_ = x
    else
      x_ = 1
    end if

    do i = 1, size(hist)
      write(unit, '(3(i0,x),i0)') i, hist(i), x_, key_
    end do

  end subroutine output_hist


end module GraphAnalysis
