module FileIO
  use Kinds
  implicit none

contains
  !     this subroutine reads in the link variables.
  !
  !     Author: Derek B. Leinweber
  !
  subroutine ReadGaugeField_cssm(filename,ur,ui,uzero,beta)

    ! IO variables
    character(len=*), intent(in)                             :: filename
    real(DP), dimension(nx,ny,nz,nt,nd,nc,nc), intent(inout) :: ur,ui
    real(DP), intent(out) :: beta,uzero

    ! Local variables
    integer                                                 :: nfig,nxf,nyf,nzf,ntf
    double precision                                        :: lastPlaq,plaqbarAvg
    integer                                                 :: ic

    !     Execution begins
    open(101,file=filename,form='unformatted',status='old',action='read')

    read (101) nfig,beta,nxf,nyf,nzf,ntf
    !write(*,*) nfig,beta,nxf,nyf,nzf,ntf

    do ic = 1, nc-1
       read (101) ur(:,:,:,:,:,ic,:)
       read (101) ui(:,:,:,:,:,ic,:)
    end do

    read (101) lastPlaq, plaqbarAvg, uzero
    close(101)

  end subroutine ReadGaugeField_cssm

  subroutine WriteCSSMLinks(filename, UR, UI)

    ! Global variables
    character(len=*),intent(in) :: filename
    real(DP),dimension(nx,ny,nz,nt,nd,nc,nc),intent(in) :: UR,UI

    ! Local variables
    real(DP) :: beta,lastPlaq,plaqbarAvg,uzero = 1.0d0
    integer :: icfg,ic

    open(101, file=filename, form='unformatted',status='replace',action='write')

    write(101) icfg,beta,nx,ny,nz,nt

    do ic = 1, nc-1
       write(101) UR(:,:,:,:,:,ic,:)
       write(101) UI(:,:,:,:,:,ic,:)
    end do

    write(101) lastPlaq, plaqbarAvg, uzero

    close(101)

  end subroutine WriteCSSMLinks

  subroutine ReadGaugeField_ildg(filename,ur,ui,uzero,beta)
    implicit none

    character(len=*)                         :: filename
    real(DP),dimension(nx,ny,nz,nt,nd,nc,nc) :: ur,ui
    real(DP)                                 :: uzero,beta

    ! Local vars
    complex(dc), dimension(:,:,:,:,:,:,:), allocatable  :: U_lxd
    integer :: matrix_len, irecl, irec
    integer :: ic, jc, mu

    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*nx*ny*nz*nt
    allocate(U_lxd(nc,nc,nd,nx,ny,nz,nt))

    open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=irecl)
    irec = 1
    read(101,rec=irec) U_lxd
    close(101)

    ! Take transpose and rearrange indices
    do mu = 1,nd
       do jc = 1,nc; do ic = 1,nc
          ur(:,:,:,:,mu,ic,jc) = real(U_lxd(jc,ic,mu,:,:,:,:))
          ui(:,:,:,:,mu,ic,jc) = aimag(U_lxd(jc,ic,mu,:,:,:,:))
       end do; end do
    end do

    ! u0 and beta are not stored, and we don't need them
    uzero = 1.0d0
    beta = 1.0d0

    deallocate(U_lxd)

  end subroutine ReadGaugeField_ildg


  !     *****************************************************************
  !     Uses Gram-Schmidt Orthonormalisation
  !     Algorithm adapted from mcsu3 by R.M. Woloshyn et. al.

  subroutine MakeSU3(RE, IM)

    real(DP), dimension(NX,NY,NZ,NT,ND,NC,NC), intent(inout) :: RE, IM

    integer :: IC, ID
    real(DP) :: NORMR(NX,NY,NZ,NT)
    real(DP) :: NORMI(NX,NY,NZ,NT)


    do ID = 1, ND
       !     Firstly, normalise the first row

       NORMR = RE(:,:,:,:,ID,1,1)**2 + IM(:,:,:,:,ID,1,1)**2 &
            + RE(:,:,:,:,ID,1,2)**2 + IM(:,:,:,:,ID,1,2)**2 &
            + RE(:,:,:,:,ID,1,3)**2 + IM(:,:,:,:,ID,1,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, 3
          RE(:,:,:,:,ID,1,IC) = RE(:,:,:,:,ID,1,IC) / NORMR
          IM(:,:,:,:,ID,1,IC) = IM(:,:,:,:,ID,1,IC) / NORMR
       enddo

       !     Now compute row2 - (row2 dot row1)row1

       NORMR = RE(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + RE(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + RE(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            + IM(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3)

       NORMI = IM(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            - RE(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            - RE(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            - RE(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3)

       do IC = 1, 3
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) &
               - (NORMR * RE(:,:,:,:,ID,1,IC) - NORMI * IM(:,:,:,:,ID,1,IC))

          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) &
               - (NORMR * IM(:,:,:,:,ID,1,IC) &
               + NORMI * RE(:,:,:,:,ID,1,IC))
       enddo


       !     Normalise the second row

       NORMR = RE(:,:,:,:,ID,2,1)**2 + IM(:,:,:,:,ID,2,1)**2 &
            + RE(:,:,:,:,ID,2,2)**2 + IM(:,:,:,:,ID,2,2)**2 &
            + RE(:,:,:,:,ID,2,3)**2 + IM(:,:,:,:,ID,2,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, 3
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) / NORMR
          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) / NORMR
       enddo

       !     Now generate row3 = row1 cross row2

       RE(:,:,:,:,ID,3,3) = RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1)

       RE(:,:,:,:,ID,3,2) = RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3)

       RE(:,:,:,:,ID,3,1) = RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2)

       IM(:,:,:,:,ID,3,3) = - RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            + RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1)

       IM(:,:,:,:,ID,3,2) = - RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            + RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3)

       IM(:,:,:,:,ID,3,1) = - RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            + RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2)

    enddo
  end subroutine MAKESU3

end module FileIO
