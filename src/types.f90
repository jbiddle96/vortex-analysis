module Types
  use Kinds
  implicit none

  integer, dimension(3) :: slice_dims

  type Arrow
    real, dimension(3) :: base, tip
    integer, dimension(3) :: direction
    integer :: val
    logical :: assigned
    ! BP_id indicates whether the vortex is associated with a BP at the
    ! tip (1st index), base (2nd index) or not associated (= -1)
    integer, dimension(2) :: BP_id

  contains
    procedure :: initialise => init_arrow
    procedure :: write_arrow
    procedure :: set_BP
    generic :: write(formatted) => write_arrow
  end type Arrow

  interface operator (==)
    module procedure eq_arrow
  end interface operator (==)

contains

  subroutine init_arrow(this, base, direction, val)
    class(Arrow) :: this
    real, dimension(3) :: base
    integer, dimension(3) :: direction
    integer :: val, dir_dim

    this%base = base
    this%direction = direction
    this%val = val
    this%assigned = .false.
    this%BP_id = -1

    this%tip = base + real(direction)

    dir_dim = maxloc(abs(direction), dim=1)

    if(this%tip(dir_dim) < 0) then
      this%tip(dir_dim) = this%tip(dir_dim) + slice_dims(dir_dim)
    end if

    if(base(dir_dim) < 0) then
      this%base(dir_dim) = base(dir_dim) + slice_dims(dir_dim)
    end if

  end subroutine init_arrow


  subroutine write_arrow(this, unit, iotype, v_list, iostat, iomsg)
    class(Arrow), intent(in) :: this    ! Object to write.
    integer, intent(in) :: unit         ! Internal unit to write to.
    character(*), intent(in) :: iotype  ! LISTDIRECTED or DTxxx
    integer, intent(in) :: v_list(:)    ! parameters from fmt spec.
    integer, intent(out) :: iostat      ! non zero on error, etc.
    character(*), intent(inout) :: iomsg  ! define if iostat non zero.

    write (unit, "(3F6.1,A,3F6.1)", IOSTAT=iostat, IOMSG=iomsg) &
        this%base, " -> ", this%tip
  end subroutine write_arrow


  function eq_arrow(a, b) result(eq)
    type(Arrow), intent(in) :: a, b
    logical :: eq

    if(all(abs(a%base - b%base) < tol) &
        .and. all(abs(a%tip - b%tip) < tol)) then
      eq = .true.
    else
      eq = .false.
    end if

  end function eq_arrow

  subroutine set_BP(this, id, loc)
    ! Assign the branching point location for this arrow
    ! Use loc to choose whether the branching point it at the tip ('t') or base ('b')
    class(Arrow) :: this
    integer, intent(in) :: id
    character(len=1), intent(in) :: loc
    select case(loc)
    case('t')
      this%BP_id(1) = id
    case('b')
      this%BP_id(2) = id
    case default
      write(*,*) 'Unrecognized branching point location, aborting'
      call exit()
    end select
  end subroutine set_BP

  logical function touches(a, b)
    ! Checks if 2 arrows are not the same and also touch
    type(Arrow) :: a, b

    touches = .false.
    if(a == b) then
      return
    end if

    if(all(abs(a%base - b%tip) < tol) &
        .or. all(abs(a%tip - b%base) < tol) &
        .or. all(abs(a%tip - b%tip) < tol) &
        .or. all(abs(a%base - b%base) < tol)) then
      touches = .true.
    end if

  end function touches

end module Types
