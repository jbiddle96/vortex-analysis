#!/bin/bash
#PBS -S /bin/bash
## Project
#PBS -P e31

## Queue
#PBS -q normal
## Number of CPU's
#PBS -l ncpus=1

## RAM
#PBS -l mem=32gb

## Maximum job time (hh:mm:ss)
#PBS -l walltime=04:00:00

## Storage directory
#PBS -l storage=gdata/e31

## Start job in submission directory
#PBS -l wd

## Put output and error in same output file
#PBS -j oe

# source activate Analysis

workdir="/g/data/e31/jb3708/data/PlottingCoordinates/Loops/UT-MCG-VO/32x64_PG/"
dirlist.py "${workdir}" "su3*.loops" ".lat" ".log" --maxfiles 8

workdir="/g/data/e31/jb3708/data/PlottingCoordinates/Loops/UT-MCG-VO/32x64_Kud01370000/"
dirlist.py "${workdir}" "RC*.loops" ".lat" ".log" --maxfiles 8

workdir="/g/data/e31/jb3708/data/PlottingCoordinates/Loops/UT-MCG-VO/32x64_Kud01378100/"
dirlist.py "${workdir}" "RC*.loops" ".lat" ".log" --maxfiles 8

python3 /home/566/jb3708/VortexPlotting/python/analyse_loops.py
