#PBS -S /bin/bash
## Project
#PBS -P e31

## Queue
#PBS -q normal
## Number of CPU's
#PBS -l ncpus=1

## RAM
#PBS -l mem=16gb

## Maximum job time (hh:mm:ss)
#PBS -l walltime=00:30:00

## Storage directory
#PBS -l storage=gdata/e31

## Start job in submission directory
#PBS -l wd

## Put output and error in same output file
#PBS -j oe

format="ildg"
max_slices=0

for that in 1 2 3 4
do
    outcfg="./$(basename "$filename")-t${that}.loops"
    outgraph="./$(basename "$filename")-t${that}.graph"
    outhist="./$(basename "$filename")-t${that}.hist"

    ~/VortexPlotting/bin/LoopAnalysis.x<<EOF
"${filename}"
32 32 32 64
$that
"${format}"
"${outcfg}"
"${outgraph}"
"${outhist}"
$max_slices
EOF
done
