#!/bin/bash
#PBS -S /bin/bash
## Project
#PBS -P e31

## Queue
#PBS -q normal
## Number of CPU's
#PBS -l ncpus=1

## RAM
#PBS -l mem=32gb

## Maximum job time (hh:mm:ss)
#PBS -l walltime=02:00:00

## Storage directory
#PBS -l storage=gdata/e31

## Start job in submission directory
#PBS -l wd

## Put output and error in same output file
#PBS -j oe

# source activate Analysis

workdir="/g/data/e31/jb3708/data/PlottingCoordinates/Loops/UT-MCG-VO/32x64_PG/"
dirlist.py "${workdir}" "su3*.graph" ".lat" ".log" --maxfiles 8 -o "graphlist.txt"
dirlist.py "${workdir}" "su3*.hist" ".lat" ".log" --maxfiles 8 -o "distlist.txt"

workdir="/g/data/e31/jb3708/data/PlottingCoordinates/Loops/UT-MCG-VO/32x64_Kud01378100/"
dirlist.py "${workdir}" "RC*.graph" ".lat" ".log" --maxfiles 8 -o "graphlist.txt"
dirlist.py "${workdir}" "RC*.hist" ".lat" ".log" --maxfiles 8 -o "distlist.txt"

python3 /home/566/jb3708/vortex_analysis/python/analyse_graphs.py
