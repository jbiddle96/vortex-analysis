#!/bin/bash

workdir="/g/data/e31/jb3708/data/CentreProjected/UT-MCG/32x64_PG/"
dirlist.py "${workdir}" "su3*" ".log" ".lat" ".dists"
file_list=${workdir}"dirlist.txt"

maxFiles=200
fileCounter=0

{
    read
    while read -r filename
    do
	qsub -v filename=$filename loops.sh

	let fileCounter=$fileCounter+1
	if [ "$fileCounter" -eq "$maxFiles" ]; then
	    break
	fi

    done
}< "$file_list"
