#!/bin/bash
#PBS -S /bin/bash
#PBS -P e31
#PBS -q normal
#PBS -l ncpus=1
#PBS -l mem=16gb
#PBS -l walltime=2:00:00
#PBS -l storage=gdata/e31
#PBS -l wd
#PBS -j oe
#"/short/e31/jb3708/data/PlottingCoordinates/Vortices/CFG95"
#"/short/e31/jb3708/data/PlottingCoordinates/TopQ/CFG95"

ulimit -s unlimited

workdir="/g/data/e31/jb3708/data/CentreProjected/UT-MCG/32x64_PG/"
outdir="./"
format="ildg"

mkdir -p $outdir

dirlist.py "${workdir}" "su3*" ".lat" ".gag" ".dists" --maxfiles 1
file_list=${workdir}"dirlist.txt"

{
    read
    while read -r incfg
    do
	outcfg="${outdir}$(basename "$incfg")"

	~/vortex_analysis/bin/Coordinates.x<<EOF
"${incfg}"
32 32 32 64
"${format}"
"${outcfg}"
EOF

	python3 ~/vortex_analysis/bin/insertlines.py "${outcfg}_singularpointssite"
	python3 ~/vortex_analysis/bin/insertlines.py "${outcfg}_singularpointstopq"
	python3 ~/vortex_analysis/bin/insertlines.py "${outcfg}_branchingpoints"
    done
}< "$file_list"
